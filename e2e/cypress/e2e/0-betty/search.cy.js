/// <reference types="cypress" />

describe("betty search", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should warn about a missing github token", () => {
    const searchTerm = "seismology zenodo doi in:readme,description";

    cy.get('[data-test="search-bar"]').type(`${searchTerm}{enter}`);
    cy.get('[data-test="notification-message"]').contains("Github Error");
  });

  it("should successfully set a github token", () => {
    const apiKey = Cypress.env("GITHUB_API_KEY");

    cy.get('[data-test="settings-button"]').click();
    cy.get('[data-test="github-token-input"]')
      .should("be.visible")
      .wait(100)
      .type(`${apiKey}{enter}`)
      .should(() => {
        expect(localStorage.getItem("authTokenGithub")).to.eq(apiKey);
      });
  });

  it("should perform a search", () => {
    const apiKey = Cypress.env("GITHUB_API_KEY");
    localStorage.setItem("authTokenGithub", apiKey);
    const searchTerm = "seismology zenodo doi in:readme,description";

    cy.get('[data-test="search-bar"]').type(`${searchTerm}{enter}`);
    cy.get('[data-test="result-table"', { timeout: 10000 }).should(
      "have.length.at.least",
      1
    );
  });

  it("should open a detailed view dialog", () => {
    const apiKey = Cypress.env("GITHUB_API_KEY");
    localStorage.setItem("authTokenGithub", apiKey);

    cy.get('[data-test="search-bar"]').type("seismology user:obspy{enter}");
    cy.get('.flex-row > [role="button"]')
      .should("be.visible")
      .invoke("text")
      .then((buttonText) => {
        cy.get('.flex-row > [role="button"]').click();
        cy.get(".text-4xl")
          .should("be.visible")
          .should("have.text", buttonText);
      });
  });

  it("should show the different logos", () => {
    cy.get('[data-test="search-logotuc"]').should("be.visible");
    cy.get('[data-test="search-logobetty"]').should("be.visible");
    cy.get('[data-test="search-logonfdi4ing"]').should("be.visible");
  });
});
