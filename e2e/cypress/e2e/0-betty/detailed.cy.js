/// <reference types="cypress" />

describe("detailed view", () => {
  context("quick", () => {
    beforeEach(() => {
      cy.visit("/");
      const apiKey = Cypress.env("GITHUB_API_KEY");
      localStorage.setItem("authTokenGithub", apiKey);
      // cy.intercept("GET", "https://raw.githubusercontent.com/**", (req) => {
      //   req.reply({
      //     statusCode: 404,
      //     body: "Not found",
      //   });
      // });
      cy.get('[data-test="search-bar"]').type("seismology user:obspy{enter}");
      cy.get('[data-test="search-loading"]');
      cy.get('.flex-row > [role="button"]', { timeout: 10000 })
        .should("be.visible")
        .click();
    });

    it("should show logos", () => {
      cy.get('[data-test="detailed-logotuc"]').should("be.visible");
      cy.get('[data-test="detailed-logonfdi4ing"]').should("be.visible");
    });

    it("should show all four buttons", () => {
      cy.get('[data-test="btn-showreadme"]').should("be.visible");
      cy.get('[data-test="btn-showdetails"]').should("be.visible");
      cy.get('[data-test="btn-showpublications"]').should("be.visible");
      cy.get('[data-test="btn-showorkg"]').should("be.visible");
    });

    it("should open and close readme", () => {
      cy.get('[data-test="btn-showreadme"]').click();
      cy.get('[data-test="detailed-readme"]');
      cy.get('[data-test="btn-showreadme"]').click();
      cy.get('[data-test="detailed-readme"]').should("not.exist");
    });

    it("should open and close details", () => {
      cy.get('[data-test="btn-showdetails"]').click();
      cy.get('[data-test="detailed-details"]');
      cy.get('[data-test="btn-showdetails"]').click();
      cy.get('[data-test="detailed-details"]').should("not.exist");
    });

    it("should open and close publications", () => {
      cy.get('[data-test="btn-showpublications"]').click();
      cy.get('[data-test="detailed-publications"]');
      cy.get('[data-test="btn-showpublications"]').click();
      cy.get('[data-test="detailed-publications"]').should("not.exist");
    });

    it("should open and close orkg", () => {
      cy.get('[data-test="btn-showorkg"]').click();
      cy.get('[data-test="detailed-orkg"]');
      cy.get('[data-test="btn-showorkg"]').click();
      cy.get('[data-test="detailed-orkg"]').should("not.exist");
    });

    it("should fetch readme and details from github", () => {
      cy.get('[data-test="btn-showreadme"]').click();
      cy.get('[data-test="detailed-readme"]')
        .find(".base > p")
        .invoke("text")
        .should("not.equal", "Null");
      cy.get('[data-test="btn-showreadme"]').click();
      cy.get('[data-test="btn-showdetails"]').click();
      cy.get('[data-test="detailed-details"]'); //the details view only appears when the request was successful, this might be unexpected behaviour
    });
  });

  context("extensive", () => {
    beforeEach(() => {
      cy.visit("/");
      const apiKey = Cypress.env("GITHUB_API_KEY");
      localStorage.setItem("authTokenGithub", apiKey);
      cy.get('[data-test="search-bar"]').type("seismology user:obspy{enter}");
      cy.get('[data-test="search-loading"]');
      cy.get('[data-test="search-finished"]', { timeout: 20000 });
      cy.get('.flex-row > [role="button"]').should("be.visible").click();
    });

    // publications only show up when the detailed view was opened after literature engine was done
    it("should fetch publications", () => {
      cy.get('[data-test="btn-showpublications"]').click();
      cy.get('[data-test="detailed-publications"]')
        .find("ul > li")
        .should("have.length.at.least", 1);
    });
  });
});
