/// <reference types="cypress" />

describe("front page links", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("leads to our paper", () => {
    cy.get('[data-test="link-paper"]')
      .should("be.visible")
      .then(($a) => {
        expect($a).to.have.attr("target", "_blank");
        expect($a).to.have.attr(
          "href",
          "https://preprints.inggrid.org/repository/view/16/"
        );
      });
  });

  it("leads to our gitlab repo", () => {
    cy.get('[data-test="link-gitlab"]')
      .should("be.visible")
      .then(($a) => {
        expect($a).to.have.attr("target", "_blank");
        expect($a).to.have.attr(
          "href",
          "https://gitlab.com/tuc-isse/public/betty-research-engine/"
        );
      });
  });

  it("opens the tutorial dialog", () => {
    cy.get('[data-test="button-tutorial"]').should("be.visible").click();
    cy.get('[data-test="dialog-title"]').contains("Info");
  });

  it("leads to docker hub", () => {
    cy.get('[data-test="link-docker"]')
      .should("be.visible")
      .then(($a) => {
        expect($a).to.have.attr("target", "_blank");
        expect($a).to.have.attr(
          "href",
          "https://hub.docker.com/r/bettyresearchengine/bettys-research-engine"
        );
      });
  });
});
