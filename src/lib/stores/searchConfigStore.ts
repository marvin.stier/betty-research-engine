import { writable } from 'svelte/store';

function createSearchConfig() {
    const { subscribe, update } = writable<{ [key: string]: any }>({
        stars: {
            value: "",
            placeholder: "0..100, 200, >1000",
            type: "text",
            name: "stars",
            set: (input: string) => update(n => ({ ...n, stars: { ...n.stars, value: input } }))
        },
        forks: {
            value: "",
            placeholder: "50..100, 200, <5",
            type: "text",
            name: "forks",
            set: (input: string) => update(n => ({ ...n, forks: { ...n.forks, value: input } }))
        },
        language: {
            value: "Any",
            placeholder: "",
            type: "options",
            name: "language",
            options: ["Any", "C", "C#", "C++", "CoffeeScript", "CSS", "Dart", "DM", "Elixir", "Go", "Groovy", "HTML", "Java", "JavaScript", "Kotlin", "Objective-C", "Perl", "PHP", "PowerShell", "Python", "Ruby", "Rust", "Scala", "Shell", "Swift", "TypeScript"],
            set: (input: string) => update(n => ({ ...n, language: { ...n.language, value: input } }))
        },
        user: {
            value: "",
            placeholder: "github, atom, electron, octokit",
            type: "text",
            name: "user",
            set: (input: string) => update(n => ({ ...n, user: { ...n.user, value: input } }))
        },
        org: {
            value: "",
            placeholder: "github",
            type: "text",
            name: "org",
            set: (input: string) => update(n => ({ ...n, org: { ...n.org, value: input } }))
        },
        size: {
            value: "",
            placeholder: "1000, >=30000, 50..120",
            type: "text",
            name: "size",
            set: (input: string) => update(n => ({ ...n, size: { ...n.size, value: input } }))
        },
        followers: {
            value: "",
            placeholder: ">=10000, 1..10",
            type: "text",
            name: "followers",
            set: (input: string) => update(n => ({ ...n, followers: { ...n.followers, value: input } }))
        },
        created: {
            value: "",
            placeholder: "YYYY-MM-DD",
            type: "text",
            name: "created",
            set: (input: string) => update(n => ({ ...n, created: { ...n.created, value: input } }))
        },
        pushed: {
            value: "",
            placeholder: "YYYY-MM-DD",
            type: "text",
            name: "pushed",
            set: (input: string) => update(n => ({ ...n, pushed: { ...n.pushed, value: input } }))
        },
        topic: {
            value: "",
            placeholder: "jekyll",
            type: "text",
            name: "topic",
            set: (input: string) => update(n => ({ ...n, topic: { ...n.topic, value: input } }))
        },
        topics: {
            value: "",
            placeholder: "5, >3",
            type: "text",
            name: "topics",
            set: (input: string) => update(n => ({ ...n, topics: { ...n.topics, value: input } }))
        },
        in: {
            value: "",
            placeholder: "readme",
            type: "text",
            name: "in",
            set: (input: string) => update(n => ({ ...n, in: { ...n.in, value: input } }))
        },
        license: {
            value: "Any",
            placeholder: "",
            type: "options",
            name: "license",
            options: ["Any", "afl-3.0", "apache-2.0", "artistic-2.0", "bsl-1.0", "bsd-2-clause", "bsd-3-clause", "bsd-3-clause-clear", "cc", "cc0-1.0", "cc-by-4.0", "cc-by-sa-4.0", "wtfpl", "ecl-2.0", "epl-1.0", "epl-2.0", "eupl-1.1", "agpl-3.0", "gpl", "gpl-2.0", "gpl-3.0", "lgpl", "lgpl-2.1", "lgpl-3.0", "isc", "lppl-1.3c", "ms-pl", "mit", "mpl-2.0", "osl-3.0", "postgresql", "ofl-1.1", "ncsa", "unlicense", "zlib"],
            set: (input: string) => update(n => ({ ...n, license: { ...n.license, value: input } }))
        },
        is: {
            value: "Any",
            placeholder: "",
            type: "options",
            name: "is",
            options: ["Any", "public", "private", "sponsorable"],
            set: (input: string) => update(n => ({ ...n, is: { ...n.is, value: input } }))
        },
        mirror: {
            value: "Any",
            placeholder: "",
            type: "options",
            name: "mirror",
            options: ["Any", "true", "false"],
            set: (input: string) => update(n => ({ ...n, mirror: { ...n.mirror, value: input } }))
        },
        template: {
            value: "Any",
            placeholder: "",
            type: "options",
            name: "template",
            options: ["Any", "true", "false"],
            set: (input: string) => update(n => ({ ...n, template: { ...n.template, value: input } }))
        },
        archived: {
            value: "Any",
            placeholder: "",
            type: "options",
            name: "archived",
            options: ["Any", "true", "false"],
            set: (input: string) => update(n => ({ ...n, archived: { ...n.archived, value: input } }))
        },
        "good-first-issues": {
            value: "",
            placeholder: ">2",
            type: "text",
            name: "good-first-issues",
            set: (input: string) => update(n => ({ ...n, "good-first-issues": { ...n["good-first-issues"], value: input } }))
        },
        "help-wanted-issues": {
            value: "",
            placeholder: ">4",
            type: "text",
            name: "help-wanted-issues",
            set: (input: string) => update(n => ({ ...n, "help-wanted-issues": { ...n["help-wanted-issues"], value: input } }))
        },
        has: {
            value: "",
            placeholder: "",
            type: "options",
            name: "has",
            options: ["Any", "funding-file"],
            set: (input: string) => update(n => ({ ...n, has: { ...n.has, value: input } }))
        },
        filename: {
            value: "",
            placeholder: "linguistic",
            type: "text",
            name: "filename",
            set: (input: string) => update(n => ({ ...n, filename: { ...n.filename, value: input } }))
        },
        extension: {
            value: "",
            placeholder: "css, py",
            type: "text",
            name: "extension",
            set: (input: string) => update(n => ({ ...n, extension: { ...n.extension, value: input } }))
        },

    });

    return {
        subscribe
    };
}

export const searchConfig = createSearchConfig();

function createSearch() {
    const { subscribe, update } = writable({
        state: "stop",
        error: "",

    });

    return {
        subscribe,
        set: (input: string) => update(n => ({ ...n, state: input })),
        setError: (input: string) => update(n => ({ ...n, error: input }))
    };
}

export const search = createSearch();


