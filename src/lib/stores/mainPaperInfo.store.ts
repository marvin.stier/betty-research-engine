import { writable } from 'svelte/store';

interface EndpointMetaInfo {
    abstracts: number,
    total: number
}

const initalValue: EndpointMetaInfo = { abstracts: 0, total: 0 };

export const endpointMetaInfo = createEndpointMetaInfo()

function createEndpointMetaInfo() {
    const { subscribe, update, set } = writable<EndpointMetaInfo>(initalValue);
    return {
        subscribe,
        reset: () => set({ abstracts: 0, total: 0 }),
        incrementAbstract: () => update(current => _incrementAbstract(current)),
        incrementTotal: () => update(current => _incrementTotal(current))
    }

}

function _incrementAbstract(current: EndpointMetaInfo) {
    current.abstracts++
    return current;
}

function _incrementTotal(current: EndpointMetaInfo) {
    current.total++
    return current;
}