import { browser } from "$app/environment";
import { derived, writable } from "svelte/store";

// OpenCitations
export let authTokenOpenCitations =
    (browser && localStorage.getItem("authTokenOpenCitation")) || "";
export function setOpenCitationsAuthToken(token: string) {
    authTokenOpenCitations = token;
    localStorage.setItem("authTokenOpenCitation", authTokenOpenCitations);
}

interface OrkgAuthState {
    normal: { accessToken: string | null };
    sandbox: { accessToken: string | null };
    [key: string]: { accessToken: string | null };
}

export const orkgAuthState = writable<OrkgAuthState>({
    normal: { accessToken: null },
    sandbox: { accessToken: null },
});
export const orkgApiVersion = writable<"normal" | "sandbox">("normal");

const ORKG_API_BASE_URLS: { [key: string]: string } = {
    normal: "https://orkg.org",
    sandbox: "https://sandbox.orkg.org",
};

const ORKG_CITES_REPOSITORY_PROPERTY: { [key: string]: string } = {
    normal: "P114000",
    sandbox: "P95000",
};

const ORKG_SUBMITTER_PROPERTY: { [key: string]: string } = {
    normal: "P119215",
    sandbox: "P101060",
};

const ORKG_BETTY_REFERENCE: { [key: string]: string } = {
    normal: "R606156",
    sandbox: "R308669",
};


export const orkgApiConfig = derived(
    [orkgAuthState, orkgApiVersion],
    ([$orkgAuthState, $orkgApiVersion]) => {
        const baseUrl = ORKG_API_BASE_URLS[$orkgApiVersion];
        const accessToken = $orkgAuthState[$orkgApiVersion].accessToken;
        const citesDoiProperty = ORKG_CITES_REPOSITORY_PROPERTY[$orkgApiVersion];
        const orkgSubmitterProperty = ORKG_SUBMITTER_PROPERTY[$orkgApiVersion];
        const orkgBettyReference = ORKG_BETTY_REFERENCE[$orkgApiVersion];
        return { baseUrl, accessToken, citesDoiProperty, orkgSubmitterProperty, orkgBettyReference };
    }
);
