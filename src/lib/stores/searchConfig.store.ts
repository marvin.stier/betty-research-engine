import { writable } from "svelte/store";

export interface SearchOption {
    name: string;
    value: string;
}

function createConfig() {
    const { subscribe, update } = writable<SearchOption[]>([]);
    const setOption = (name: string, value: string) =>
        update((current) => [
            ...current.filter((option) => option.name !== name),
            { name, value },
        ]);
    const removeOption = (name: string) =>
        update((current) =>
            current.filter((option) => option.name !== name)
        );
    return {
        subscribe,
        setOption,
        removeOption,
    };
}

export const searchConfig = createConfig();
