import type { INotification } from "$lib/types/clientNotification.type";
import { writable } from "svelte/store";

export const notificationStore = writable<INotification>({});