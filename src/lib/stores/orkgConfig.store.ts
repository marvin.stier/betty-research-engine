import { NodeType } from "$lib/controller/orkg.api";
import type {
    BaseInput,
    ResourcesInput,
    OrkgPublications,
    OrkgSinglePublication,
} from "$lib/types/orkg.type";

export let orkgConfig: { [key: string]: BaseInput | ResourcesInput } = {};

export function setOrkgConfig(newConfig: {
    [key: string]: BaseInput | ResourcesInput;
}) {
    orkgConfig = newConfig;
}

export function getOrkgConfig() {
    return orkgConfig;
}

export function resetOrkgConfig() {
    orkgConfig = {};
}

let orkgRepositoryId: string = "";
export function setOrkgRepositoryId(repositoryId: string): void {
    orkgRepositoryId = repositoryId;
}

export function getOrkgRepositoryId(): string {
    return orkgRepositoryId;
}

export let orkgPub: OrkgPublications = {
    publications: [
        {
            authors: [
                {
                    value: "",
                    id: "P27",
                    instanceId: "",
                    inputType: "select",
                    nodeType: NodeType.literals, //No idea what this should be. They save authors as both resources and literals. Since website does literals I'm going with that
                    name: "Authors",
                    dataType: "xsd:string",
                    class: "Author",
                    statementId: "",
                    modified: false,
                    show: true,
                    delete: false,
                    options: [],
                    optionsDict: {},
                },
            ],
            doi: {
                value: "",
                id: "P26",
                instanceId: "",
                inputType: "text",
                nodeType: NodeType.literals,
                name: "DOI",
                dataType: "xsd:string",
                class: "",
                statementId: "",
                modified: false,
                show: true,
                delete: false,
            },
            name: {
                // This is just the name and does not need to be added as a statement
                value: "",
                id: "",
                instanceId: "label",
                inputType: "text",
                nodeType: NodeType.literals,
                name: "Name",
                dataType: "xsd:string",
                class: "Paper",
                statementId: "",
                modified: false,
                show: true,
                delete: false,
            },
            year: {
                value: "",
                id: "P29",
                instanceId: "",
                inputType: "number",
                nodeType: NodeType.literals,
                name: "Publication year",
                dataType: "xsd:string",
                class: "",
                statementId: "",
                modified: false,
                show: true,
                delete: false,
            },
            url: {
                value: "",
                id: "url",
                instanceId: "",
                inputType: "url",
                nodeType: NodeType.literals,
                name: "Url",
                dataType: "xsd:anyURI",
                class: "",
                statementId: "",
                modified: false,
                show: true,
                delete: false,
            },
            abstract: {
                value: "",
                id: "P37015",
                instanceId: "",
                inputType: "text",
                nodeType: NodeType.literals,
                name: "Abstract",
                dataType: "xsd:string",
                class: "",
                statementId: "",
                modified: false,
                show: true,
                delete: false,
            },
        },
    ],
};

// TODO: Change this
export function resetOrkgPub() {
    orkgPub = {
        publications: [
            {
                authors: [
                    {
                        value: "",
                        id: "P27",
                        instanceId: "",
                        inputType: "select",
                        nodeType: NodeType.literals, //No idea what this should be. They save authors as both resources and literals. Since website does literals I'm going with that
                        name: "Authors",
                        dataType: "xsd:string",
                        class: "Author",
                        statementId: "",
                        modified: false,
                        show: true,
                        delete: false,
                        options: [],
                        optionsDict: {},
                    },
                ],
                doi: {
                    value: "",
                    id: "P26",
                    instanceId: "",
                    inputType: "text",
                    nodeType: NodeType.literals,
                    name: "DOI",
                    dataType: "xsd:string",
                    class: "",
                    statementId: "",
                    modified: false,
                    show: true,
                    delete: false,
                },
                name: {
                    // This is just the name and does not need to be added as a statement
                    value: "",
                    id: "",
                    instanceId: "label",
                    inputType: "text",
                    nodeType: NodeType.literals,
                    name: "Name",
                    dataType: "xsd:string",
                    class: "Paper",
                    statementId: "",
                    modified: false,
                    show: true,
                    delete: false,
                },
                year: {
                    value: "",
                    id: "P29",
                    instanceId: "",
                    inputType: "number",
                    nodeType: NodeType.literals,
                    name: "Publication year",
                    dataType: "xsd:string",
                    class: "",
                    statementId: "",
                    modified: false,
                    show: true,
                    delete: false,
                },
                url: {
                    value: "",
                    id: "url",
                    instanceId: "",
                    inputType: "url",
                    nodeType: NodeType.literals,
                    name: "Url",
                    dataType: "xsd:anyURI",
                    class: "",
                    statementId: "",
                    modified: false,
                    show: true,
                    delete: false,
                },
                abstract: {
                    value: "",
                    id: "P37015",
                    instanceId: "",
                    inputType: "text",
                    nodeType: NodeType.literals,
                    name: "Abstract",
                    dataType: "xsd:string",
                    class: "",
                    statementId: "",
                    modified: false,
                    show: true,
                    delete: false,
                },
            },
        ],
    };
}

export function getOrkgPub() {
    return orkgPub;
}

export function setOrkgPub(newPub: OrkgPublications) {
    orkgPub = newPub;
}

export function addOrkgPublication(publication: OrkgSinglePublication) {
    orkgPub.publications.push(publication);
}

export function removeOrkgPublication() {
    orkgPub.publications.pop();
}

// Key is the name of the paper
// Value is the resource id
let publicationMap = new Map<string, string>();

export function setValuePublicationMap(key: string, value: string): void {
    publicationMap.set(key, value);
}

export function getValuePublicationMap(key: string) {
    return publicationMap.get(key);
}

export function hasValuePublicationMap(key: string): boolean {
    return publicationMap.has(key);
}

export function clearPublicationMap(): void {
    publicationMap.clear();
}
