export default class ZenodoUtil {
  /**
   * Gets the doi from a Readme file
   * @param readme Readme file
   * @returns doi if found
   */
  static getZenodoDoi(readme: string) {
    try {
      let doiIdPattern = /zenodo.(\d+).?/;
      let zenodoLinkPattern = /zenodo.org\/record\/(\d+)/;

      // Temp Doi
      var doi = "error";

      let match1 = doiIdPattern.exec(readme);
      if (match1 !== null) {
        doi = match1[1];
        return doi;
      }
      let match2 = zenodoLinkPattern.exec(readme);
      if (match2 !== null) {
        doi = match2[1];
        return doi;
      }
      return doi;
    } catch (e) {
      console.log(`Error ${e}`);
      return "error";
    }
  }

  /**
   * Gets the doi from a Readme file
   * @param readme Readme file
   * @returns doi if found
   */
  static getZenodoDoi2(readme: string) {
    //TODO:why is there a different version?
    try {
      let zenodoBadgePattern = /zenodo.org\/badge\/DOI\/10.5281\/zenodo.(\d+)/;
      let doiIdPattern = /zenodo.(\d+).?/;
      let zenodoLinkPattern = /zenodo.org\/record\/(\d+)/;
      let zenodoLatestPattern =
        /zenodo.org\/badge\/latestdoi\/\d+(?:(?:\/\w+-*\w+)*)?/;

      // Temp Doi
      var doi = "error";

      let match0 = zenodoBadgePattern.exec(readme);
      if (match0 !== null) {
        doi = match0[1];
        return doi;
      }
      let match1 = doiIdPattern.exec(readme);
      if (match1 !== null) {
        doi = match1[1];
        // console.log('REGEX MATCH 1 ' + doi);
        return doi;
      }
      let match2 = zenodoLinkPattern.exec(readme);
      if (match2 !== null) {
        doi = match2[1];
        // console.log('REGEX MATCH 2 ' + doi);
        return doi;
      }
      // // Blocked by cors
      // let match3 = "https://" + zenodo_latest_pattern.exec(readme);
      // if (match3 !== null) {
      //     let tmpUrl = match3[0];

      //     fetch(tmpUrl).then((res) => {
      //         // TODO get real redirect

      //         let realUrl = res.url;
      //         console.log(tmpUrl);
      //         console.log(realUrl);
      //         console.log(res.redirected)
      //         doi = realUrl.split("/").slice(-1)[0];
      //         console.log('REGEX MATCH 3 ' + doi);
      //         return doi;
      //     }).then((doi_tmp) => {
      //         doi = doi;
      //     })
      // }

      return doi;
    } catch (e) {
      console.log(`Error ${e}`);
      return "error";
    }
  }

  /**
   * Check if the readme contains a valid zenodo badge
   * @param readme Readme
   * @returns true if zenodo doi badge in readme, else false
   */
  static zenodoBadgeReadme(readme: string) {
    let zenodoBadgePattern = /zenodo.org\/badge\/DOI\/10.5281\/zenodo.(\d+)/;
    let match0 = zenodoBadgePattern.exec(readme);
    if (match0 !== null) {
      return true;
    }
    return false;
  }
}
