import type { NodeType } from "$lib/controller/orkg.api";

export interface OrkgSinglePublication {
    [key: string]: BaseInput | ResourcesInput | ResourcesInput[],
    authors: ResourcesInput[]; // This ones should be resources
    doi: BaseInput;
    name: BaseInput;
    year: BaseInput;
    url: BaseInput;
    abstract: BaseInput;
}
export interface OrkgPublications {
    publications: OrkgSinglePublication[];
}

export interface BaseInput {
    value: string;
    id: string;
    instanceId: string;
    inputType: string;
    nodeType: NodeType;
    name: string;
    dataType: string;
    class: string;
    statementId: string;
    modified: boolean;
    show: boolean;
    delete: boolean;
}

export interface ResourcesInput extends BaseInput {
    options: any[];
    optionsDict: { [key: string]: string };
}


export interface OrkgInput {
    [key: string]: BaseInput | ResourcesInput;
    name: BaseInput;
    citation: ResourcesInput;
    codeRepository: BaseInput;
    description: BaseInput;
    programmingLanguage: ResourcesInput;
    referencePublication: BaseInput;
    dateCreated: BaseInput;
    dateModified: BaseInput;
    readme: BaseInput;
}

