export interface Search {
    query: string,
    totalRepos: number
    repositories: Map<string, Repository>
}

export interface Repository {
    name: string,
    url: string,
    description: string,
    stars: number,
    forks: number,
    readme: string,
    readmeUrl: string | undefined,
    publications: Publication[],
    createdAt: Date,
    updatedAt: Date,
    language: string,
    homepage: string,
    mainPaper: MainPaper | undefined,
    amountPublications: Map<string, number>,
    gotOpenCitationsPub: boolean,
    gotDataCitePub: boolean,
    agentQueryTerm: AgentQueryTerm
}

export interface Publication {
    doi: string,
    name: string,
    source: string,
    authorNames: string[],
    externalIds: {} | undefined,
    abstract: string | undefined,
    venue: string | undefined,
    year: number | undefined,
    referenceCount: number | undefined,
    citationCount: number | undefined,
    fieldsOfStudy: string[] | undefined,
    publicationTypes: string[] | undefined,
    publicationDate: Date | undefined,
    journal: {} | undefined | string,
    url: string[] | undefined
}

export interface MainPaper {
    doi: string,
    title: string,
    journal: string,
    // authorNames: string[], // This is commented because the authors are objects
    dateReleased: Date | undefined,
    abstract: string,
    citationsArray: Array<string> | undefined
}

export interface AgentQueryTerm {
    zenodo: string,
    openAlex: string,
    openCitations: string,
    dataCite: string
}