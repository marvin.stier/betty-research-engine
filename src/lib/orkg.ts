export const fetchPages = async (
  url: string,
  pageSize: number = 30,
  maxPages: number = 10,
  additionalParams: Record<string, string> = {}
) => {
  let content: any[] = [];

  let page = 0;
  let json: any;
  do {
    const params = new URLSearchParams({
      size: pageSize.toString(),
      page: page.toString(),
      ...additionalParams,
    });
    const res = await fetch(`${url}?${params}`);
    json = await res.json();
    content = [...content, ...json.content];
    page++;
  } while (page < maxPages && page < json.totalPages);

  return content;
};

/**
 * Takes an orkg entry array and removes duplicates, leaving those
 * with the highest share count.
 *
 * @param content orkg entries to filter
 * @param ignoreCase whether to ignore the label's case
 * @returns orkg entries without duplicates
 */
const removeDuplicates = (
  content: any[],
  ignoreCase: boolean = false
): any[] => {
  const filteredContent: any[] = Object.values(
    content.reduce((acc, entry) => {
      const key = ignoreCase ? entry.label.toLowerCase() : entry.label;
      if (!acc[key] || entry.shared > acc[key].shared) {
        acc[key] = entry;
      }
      return acc;
    }, {})
  );

  return filteredContent;
};

export const fetchLanguages = async (
  baseUrl: string,
  pageSize: number = 30,
  maxPages: number = 10
) => {
  return fetchPages(
    `${baseUrl}/api/classes/C29016/resources/`,
    pageSize,
    maxPages
  );
};

export const fetchCitations = async (
  baseUrl: string,
  pageSize: number = 30,
  maxPages: number = 10
) => {
  return fetchPages(
    `${baseUrl}/api/classes/Citation/resources/`,
    pageSize,
    maxPages
  );
};

interface OrkgLanguage {
  id: string;
  label: string;
  shared: number;
}

export const parseLanguages = (content: any[]): OrkgLanguage[] => {
  const languages = removeDuplicates(content, true)
    .map<OrkgLanguage>((entry: any) => ({
      id: entry.id,
      label: entry.label,
      shared: entry.shared,
    }))
    .sort((a: any, b: any) => a.label.localeCompare(b.label));
  return languages;
};

interface OrkgCitation {
  id: string;
  label: string;
  shared: number;
}

export const parseCitations = (content: any[]): OrkgCitation[] => {
  const citations = removeDuplicates(content, false).map<OrkgCitation>(
    (entry: any) => ({
      id: entry.id,
      label: entry.label,
      shared: entry.shared,
    })
  );
  return citations;
};
