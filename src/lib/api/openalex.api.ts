import Bottleneck from "bottleneck";

export class OpenAlexAPI {
    private static BASE_URL = 'https://api.openalex.org';
    private static instance: OpenAlexAPI;

    private limiter: Bottleneck;

    private constructor() {
        //TODO:set openAlex rate limit parameters
        this.limiter = new Bottleneck({
            minTime: 333
        });
    }

    public static getInstance(): OpenAlexAPI {
        if (!OpenAlexAPI.instance)
            OpenAlexAPI.instance = new OpenAlexAPI();
        return OpenAlexAPI.instance;
    }

    public async fetchCitations(fullName: string): Promise<any> {
        let gitURI = `github.com/${fullName}`;
        //https://api.openalex.org/works?filter=fulltext.search:%22github.com/obspy/obspy%22
        const queryString = `filter=fulltext.search:${encodeURIComponent(`"${gitURI}"`)}`;
        const url = `${OpenAlexAPI.BASE_URL}/works?${queryString}`;

        const res = await this.limiter.schedule(
            () => fetch(url)
        );

        if (!res.ok) {
            return Promise.reject('error fetching openAlex citations?');
        }

        const json = await res.json();
        return json;
    }
}
