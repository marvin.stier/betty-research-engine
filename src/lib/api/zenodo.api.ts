import { authTokenZenodo } from "$lib/stores/githubResults.store";
import Bottleneck from "bottleneck";
import { get } from "svelte/store";

export class ZenodoAPI {
  private static BASE_URL = "https://zenodo.org/api";
  private static instance: ZenodoAPI;

  private limiter: Bottleneck;
  private brokerLimiter: Bottleneck;

  private constructor() {
    //TODO:set zenodo rate limit parameters
    this.limiter = new Bottleneck({
      minTime: 333,
    });
    this.brokerLimiter = new Bottleneck({
      minTime: 333,
    });
  }

  public static getInstance(): ZenodoAPI {
    if (!ZenodoAPI.instance) ZenodoAPI.instance = new ZenodoAPI();
    return ZenodoAPI.instance;
  }

  public async getRecord(doi: string): Promise<any> {
    const url = `${ZenodoAPI.BASE_URL}/records/${doi}?access_token=${get(
      authTokenZenodo
    ).valueOf()}`;
    const res = await this.limiter.schedule(() => fetch(url));

    if (res.ok) {
      const json = await res.json();
      return json;
    }

    return Promise.reject("error fetching record?");
  }

  public async getAllCitations(conceptDoi: string): Promise<any> {
    // This request returns all the citations
    const url =
      "https://zenodo-broker.web.cern.ch/api/relationships?page=1&id=" +
      conceptDoi +
      "&scheme=doi&group_by=version&relation=isCitedBy&sort=-mostrecent&size=9999";

    let res = await this.brokerLimiter.schedule(() => fetch(url));

    // if (res.ok) {
    //     let json = await res.json();
    //     return json;
    // }
    let json = await res.json();
    return json;

    return Promise.reject("error fetching citations?");
  }
}
