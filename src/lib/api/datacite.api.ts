import type { MainPaper, Publication } from "$lib/types/githubResults.type";
import Bottleneck from "bottleneck";

class DataCiteAPI {
  //TODO:set rate limit parameters
  static limiter = new Bottleneck({
    minTime: 333,
  });

  static getCitations = async (citationDoi: string): Promise<Publication> => {
    // TODO Add Access token
    const url = `https://api.datacite.org/dois/${citationDoi}`;
    let res = await DataCiteAPI.limiter.schedule(() => fetch(url));
    let json = await res.json();

    let doi = json.data.attributes.doi;
    let title = json.data.attributes.titles[0].title;
    let journal = json.data.attributes.publisher;
    let date = new Date(json.data.attributes.dates[0].date);
    let abstract = json.data.attributes.descriptions[0].description;
    let link = json.data.attributes.url;

    let authorsTemp = json.data.attributes.creators;

    // next up are the Authors
    let authorNames: string[] = authorsTemp.map((author: any) => author.name);

    // Get citations
    // Only parse citations if there are citations
    const citations: string[] = json.data.relationships.citations.data
      .filter((cit: any) => cit.type === "dois")
      .map((cit: any) => cit.id);
    let citationCount = citations.length;

    return {
      doi: doi,
      name: title,
      source: "Data Cite",
      authorNames: authorNames,
      externalIds: undefined,
      abstract: abstract,
      venue: undefined,
      year: date.getFullYear(),
      referenceCount: undefined,
      citationCount: citationCount,
      fieldsOfStudy: undefined,
      publicationTypes: undefined,
      publicationDate: undefined,
      journal: journal,
      url: link,
    };
  };

  static getMainPaper = async (url: string): Promise<MainPaper> => {
    let res = await fetch(url);
    let json = await res.json();
    let doi = json.data.attributes.doi;
    let title = json.data.attributes.titles[0].title;
    let journal = json.data.attributes.publisher;
    let date = new Date(json.data.attributes.dates[0].date);
    let abstract = json.data.attributes.descriptions[0].description;

    // Get citations
    let citations: Array<string> = [];
    // Only parse citations if there are citations
    let citationsTemp = json.data.relationships.citations.data;
    for (let cit of citationsTemp) {
      if ((cit.type = "dois")) {
        citations.push(cit.id);
      }
    }

    return {
      doi: doi,
      title: title,
      journal: journal,
      dateReleased: date,
      abstract: abstract,
      citationsArray: citations,
    };
  };
}

export default DataCiteAPI;
