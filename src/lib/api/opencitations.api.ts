import { authTokenOpenCitations } from "$lib/stores/authTokens.store";
import Bottleneck from "bottleneck";

export class OpenCitationsAPI {
    private static BASE_URL = '';
    private static instance: OpenCitationsAPI;

    private limiter: Bottleneck;

    private constructor() {
        //TODO:set openCitations rate limit paramters
        this.limiter = new Bottleneck({
            minTime: 333
        });
    }

    public static getInstance(): OpenCitationsAPI {
        if (!OpenCitationsAPI.instance)
            OpenCitationsAPI.instance = new OpenCitationsAPI();
        return OpenCitationsAPI.instance;
    }

    public async getCitations(doi: string): Promise<any> {
        // Only make the call if a doi was found
        if (doi === "error") {
            // If the paper was not found, it is needed to be stored as having 0 citations to be sorted
            // githubResults.setAmountPublications(repo.name, 'openCitations', 0);
            Promise.reject('No citations found');
        }
        // This request gets the concept_doi
        // TODO Add Access token
        const url = 'https://opencitations.net/index/api/v1/metadata/' + doi;
        let res = await this.limiter.schedule(
            () => fetch(url, {
                headers: {
                    Authorization: authTokenOpenCitations
                }
            })
        );
        if (!res.ok) {
            return Promise.reject('error fetching open citation citations');
        }

        let json = await res.json();
        if (json.length <= 0) {
            // If the paper was not found, it is needed to be stored as having 0 citations to be sorted
            // githubResults.setAmountPublications(repo.name, 'openCitations', 0);
            Promise.reject('No citations found');
        }
        return json;
    }
}
