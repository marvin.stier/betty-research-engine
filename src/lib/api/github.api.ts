import { authTokenGithub } from "$lib/stores/githubResults.store";
import type { AgentQueryTerm, Repository } from "$lib/types/githubResults.type";
// import type { Repository } from "$lib/types/newTypes.type";
import Bottleneck from "bottleneck";
import { get } from "svelte/store";

interface RepositoryInfo {
  full_name: string;
  html_url: string;
  stargazers_count: number;
  forks_count: number;
  created_at: string; //Date
  updated_at: string; //Date
  language: string | null;
  homepage: string | null;
}

interface Search {
  total_count: number;
  incomplete_results: boolean;
  items: RepositoryInfo[];
}

const mapToRepository = (repositoryInfo: RepositoryInfo): Repository => {
  // The query terms used by the agents
  // TODO: remove from this step or change so that agents can register their query terms themselves
  let agentQuery: AgentQueryTerm = {
    zenodo: "",
    openAlex: "",
    openCitations: "",
    dataCite: "",
  };

  return {
    name: repositoryInfo.full_name,
    url: repositoryInfo.html_url,
    stars: repositoryInfo.stargazers_count,
    forks: repositoryInfo.forks_count,
    createdAt: new Date(repositoryInfo.created_at),
    updatedAt: new Date(repositoryInfo.updated_at),
    language: repositoryInfo.language ?? "", // maybe change to undefined?
    homepage: repositoryInfo.homepage ?? "", // maybe change to undefined?
    publications: [],
    mainPaper: undefined,
    agentQueryTerm: agentQuery,
    amountPublications: new Map<string, number>(),
    gotDataCitePub: false,
    gotOpenCitationsPub: false,
    readme: "",
  };
};

export class GitHubAPI {
  private static BASE_URL = "https://api.github.com";
  private static BASE_URL_USERCONTENT = "https://raw.githubusercontent.com";
  private static instance: GitHubAPI;

  private searchLimiter: Bottleneck;
  private coreLimiter: Bottleneck;
  private rawLimiter: Bottleneck;

  private constructor() {
    //TODO:set gh rate limit parameters
    this.searchLimiter = new Bottleneck({
      minTime: 333,
    });
    this.coreLimiter = new Bottleneck({
      minTime: 333,
    });
    this.rawLimiter = new Bottleneck({
      minTime: 333,
    });
  }

  public static getInstance(): GitHubAPI {
    if (!GitHubAPI.instance) GitHubAPI.instance = new GitHubAPI();
    return GitHubAPI.instance;
  }

  public static testTypedSearch(): Promise<Repository[]> {
    return fetch(
      'https://api.github.com/search/repositories?q="Seismology" in:readme,description&page=0&per_page=10'
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        return response.json().then((data) => data as Search);
      })
      .then((search) => search.items.map(mapToRepository));
  }

  //TODO:define return type?
  //TODO:change failure return
  private async callSearch(
    searchTerm: string,
    queryExtension: string,
    queryLimit: string,
    page: number,
    pageSize: number
  ): Promise<any> {
    const query = encodeURIComponent(`"${searchTerm}" ${queryLimit}`);
    const url = `${GitHubAPI.BASE_URL}/search/${queryExtension}`;
    try {
      const res = await this.searchLimiter.schedule(() =>
        fetch(`${url}?q=${query}&page=${page}&per_page=${pageSize}`, {
          headers: {
            "Content-Type": "application/json",
            Authorization: `token ${get(authTokenGithub).valueOf()}`,
          },
        })
      );
      const json = await res.json();
      return json;
    } catch (e) {
      return 0;
    }
  }

  private async callCore(path: string): Promise<Response> {
    const url = `${GitHubAPI.BASE_URL}${path}`;
    const res = await this.coreLimiter.schedule(() =>
      fetch(`${url}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `token ${get(authTokenGithub).valueOf()}`,
        },
      })
    );
    return res;
  }

  private async callUserContentAPI(path: string): Promise<Response> {
    const url = `${GitHubAPI.BASE_URL_USERCONTENT}${path}`;
    const res = await this.rawLimiter.schedule(() => fetch(url));
    //TODO:change to text returned?
    return res;
  }

  public async getTotalReposAmount(
    searchTerm: string,
    queryExtension: string,
    queryLimit: string
  ): Promise<number> {
    const json = await this.callSearch(
      searchTerm,
      queryExtension,
      queryLimit,
      1,
      1
    );
    return json.total_count;
  }

  public async getPage(
    searchTerm: string,
    queryExtension: string,
    queryLimit: string,
    page: number,
    pageSize: number
  ): Promise<Repository[]> {
    const json = await this.callSearch(
      searchTerm,
      queryExtension,
      queryLimit,
      page,
      pageSize
    );
    // map results to repositories
    return json.items.map((repo: any) => ({
      name: repo.full_name,
      url: repo.html_url,
      stars: repo.stargazers_count,
      forks: repo.forks_count,
      readme: undefined,
      publications: undefined,
      createdAt: new Date(repo.created_at),
      updatedAt: new Date(repo.updated_at),
      language: repo.language,
      homepage: repo.homepage,
      mainPaper: undefined,
      amountPublications: undefined,
      semanticPaperId: undefined,
      gotSemanticPub: undefined,
      gotOpenCitationsPub: undefined,
      gotDataCitePub: undefined,
      agentQueryTerm: undefined,
    }));
  }

  public async getCitationCff(fullName: string): Promise<string> {
    const path = `/${fullName}/master/CITATION.cff`;
    const res = await this.callUserContentAPI(path);
    if (res.ok) {
      return res.text();
    }

    return Promise.reject("No citation CFF found.");
  }

  public async getReadme(fullName: string): Promise<string> {
    const readmeArr = ["README.md", "README.rst", "readme.md"];
    for (const readmeType of readmeArr) {
      const path = `/${fullName}/master/${readmeType}`;
      const res = await this.callUserContentAPI(path);
      if (res.ok) {
        return res.text();
      }
    }

    let path = `/${fullName}/readme`;
    let res = await this.callCore(path);
    //TODO:why not res.ok?
    if (res.status === 200) {
      let json = await res.json();
      let readme = Buffer.from(json.content).toString("base64");
      return readme;
    }

    return Promise.reject("No readme found.");
  }

  public async getFiles(fullName: string) {
    const path = `/repos/${fullName}/contents`;
    const res = await this.callCore(path);

    if (res.ok) {
      const json = await res.json();
      const files = json
        .filter((item: any) => item.type === "file")
        .map((file: any) => ({
          file_name: file.name,
          download_url: file.download_url,
        }));
      return files;
    }

    return Promise.reject("No files found.");
  }

  public async getReadmeFromFiles(
    files: [
      {
        file_name: string;
        download_url: string;
      }
    ],
    fullName: string
  ): Promise<[string, string]> {
    const readmeArr = ["README.md", "README.rst", "readme.md"]; //TODO:extend list, does not affect number of calls made, therefore can be extensive
    for (const readmeType of readmeArr) {
      const file = files.find((file) => file.file_name === readmeType);
      if (file !== undefined) {
        const res = await fetch(file.download_url); // TODO: use limiter
        if (res.ok) {
          return [await res.text(), file.download_url];
        }
      }
    }

    let path = `/${fullName}/readme`;
    let res = await this.callCore(path);
    //TODO:why not res.ok?
    if (res.status === 200) {
      let json = await res.json();
      let readme = Buffer.from(json.content).toString("base64");
      return [readme, GitHubAPI.BASE_URL + path];
    }

    return ["Null", "Null"]; //TODO: use Promise.rejected
  }

  public async getCitationCFFFromFiles(
    files: [
      {
        file_name: string;
        download_url: string;
      }
    ]
  ): Promise<string> {
    const file = files.find((file) => file.file_name === "CITATION.cff");
    if (file === undefined) {
      return "nothing found"; //TODO: return rejected promise
    }

    const res = await fetch(file.download_url); // TODO:use limiter
    if (res.ok) {
      return res.text();
    }

    return "nothing found";
  }
}
