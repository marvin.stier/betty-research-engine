import { githubResults } from '$lib/stores/githubResults.store';
import type { Search, Repository, Publication } from '$lib/types/githubResults.type';
import Manager from './Manager';
import { authTokenZenodo } from '$lib/stores/githubResults.store';
import { get } from "svelte/store";
import ZenodoUtil from '$lib/util/zenodo.util';
import { ZenodoAPI } from '$lib/api/zenodo.api';


export class zenodoAgent {
    id: number;
    stop: boolean;
    static zenodoAccessToken: string = get(authTokenZenodo).valueOf();

    constructor(id: number) {
        this.id = id;
        this.stop = false;
    }

    /**
     * get the Doi and then get all Citations
     * @param repo Repository
     */
    async startZenodoSearch(repo: Repository) {
        let zenodoDoi = ZenodoUtil.getZenodoDoi2(repo.readme);
        let statusCode = 0;
        let status = true;
        // Only make the call to Zenodo if the doi was found
        if (zenodoDoi !== "error" && !this.stop) {
            statusCode = await this.getAllCitations(repo, zenodoDoi);
        }
        if (!this.stop) {
            status = statusCode !== 429;
            Manager.statusZenodoAgent(status, repo);
        }

    }

    /**
     * getsAllCitations for a repo and writes them to the store
     * @param repoIdx Index of the repo in the store
     * @param zenodoDoi the doi which was found in the readme
     * @returns status of the request
     */
    private async getAllCitations(repo: Repository, zenodoDoi: string) {
        if (this.stop) {
            return 200;
        }
        let repoName = repo.name;

        let statusCode = 0;
        let amountCitations = 0;
        try {
            const recordJson = await ZenodoAPI.getInstance().getRecord(zenodoDoi);
            let conceptDoi = recordJson.conceptdoi;
            // Checks to see if the doi is from the repo
            // Remove all non alphanumeric symbols from the strings 
            let titleInZenodo = recordJson.metadata.title.toLowerCase().replace(/[^0-9a-z]/gi, '');
            let shortRepoName = repoName.split('/')[1].toLowerCase().replace(/[^0-9a-z]/gi, '');
            let nameInZenodoTitle = titleInZenodo.includes(shortRepoName);
            //let zenodoTitleInReadme = repo.readme.toLowerCase().replace(/[^0-9a-z]/gi, '').includes(titleInZenodo); //Too many false positives
            let doiFromBadge = ZenodoUtil.zenodoBadgeReadme(repo.readme);
            // Check if the repoName is the same as in Zenodo
            if (!(nameInZenodoTitle || doiFromBadge)) {
                githubResults.setAmountPublications(repoName, 'zenodo', 0);
                return statusCode;
            }
            const citationsJson = await ZenodoAPI.getInstance().getAllCitations(conceptDoi);

            let citationJson = citationsJson.hits.hits;
            amountCitations = citationJson.length;
            // we loop over each citation which are defined in json.hits.hits
            for (const citation of citationJson) {
                let urlArray = new Array();
                let citationDoi = ""
                let citationLink = ""
                // since doi is an identifier we have to check the Identifiers
                for (const item of citation.metadata.Source.Identifier) {
                    // zenodo has either "ads" or "doi" as an identifier for the citations, this is still a relic from the python api
                    // but is still being kept to provide an alternative in case the doi is not available, which happens often
                    if (item.IDScheme == "ads" && item.IDURL && citationLink === "") {
                        citationLink = item.IDURL
                    }
                    else if (item.IDScheme == "doi" && item.IDURL && citationDoi === "") {
                        citationDoi = item.IDURL
                    }
                }
                for (const item of citation.metadata.Source.Identifier) {
                    urlArray.push(item.IDURL);
                }

                // next up are the Authors
                let authorArr = new Array() as Array<string>
                // the Author list can be found under citation.metadata.Source.Creator but can also be undefined for a citation, so 
                // it has to be checked to avoid an error
                if (!(citation.metadata.Source.Creator === undefined)) {

                    for (const item of citation.metadata.Source.Creator) {
                        authorArr.push(item.Name);
                    }
                }
                // Some publications have neither name nor doi
                if (typeof (citation.metadata.Source.Title) === 'undefined') continue
                let pub: Publication = {
                    doi: citationDoi,
                    name: citation.metadata.Source.Title,
                    source: "Zenodo",
                    authorNames: authorArr,
                    externalIds: undefined,
                    abstract: undefined,
                    venue: undefined,
                    year: undefined,
                    referenceCount: undefined,
                    citationCount: undefined,
                    fieldsOfStudy: undefined,
                    publicationTypes: undefined,
                    publicationDate: undefined,
                    journal: undefined,
                    url: urlArray
                };
                // Some publications have almost all values as undefined which throws an error in the store
                try {
                    githubResults.addPublication(repoName, pub);

                } catch (error) {
                    console.error(error)
                }
            }
        }
        catch (e) {
            console.error(e)
        }
        // Update the agent Query term of the repo
        repo.agentQueryTerm.zenodo = '10.5281/zenodo.' + zenodoDoi;
        githubResults.updateRepo(repo);

        githubResults.setAmountPublications(repoName, 'zenodo', amountCitations);
        return statusCode;
    }

    /**
     * Sets the stop variable value
     * @param value  - new value of stop variable
     */
    setStop(value: boolean) {
        this.stop = value;
        console.log(`Zenodo Agent ${this.id} set to ${this.stop}`);
    }
}

