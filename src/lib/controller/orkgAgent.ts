import type { Repository } from "$lib/types/githubResults.type";

import { notificationStore } from "$lib/stores/clientNotification.store";
import { NotificationType } from "$lib/types/clientNotification.type";
import { OrkgAPI, HttpRequestsMethods, NodeType } from "./orkg.api";
import {
    getOrkgConfig,
    setOrkgConfig, getOrkgRepositoryId, setOrkgRepositoryId,
    getOrkgPub, setOrkgPub,
    getValuePublicationMap, setValuePublicationMap,
    hasValuePublicationMap, clearPublicationMap
} from "$lib/stores/orkgConfig.store";
import {
    createAuthor, createLiteralInput, createResourceInput, getDoi,
    createOrkgConfig
} from "$lib/controller/orkg.utils";

import type { BaseInput, OrkgSinglePublication, ResourcesInput } from "$lib/types/orkg.type";

import type { Statement } from "$lib/controller/orkg/statements/statementElements";
import { parseJsonStatement, createTemplateComponent } from "$lib/controller/orkg/statements/statementElements";
import { fetchCitations, fetchLanguages, parseCitations, parseLanguages } from "$lib/orkg";
import { orkgApiConfig, orkgApiVersion } from "$lib/stores/authTokens.store";
import { get } from "svelte/store";

export class OrkgAgent {

    private static instance: OrkgAgent;
    private orkgApi: OrkgAPI = OrkgAPI.getInstance();
    private orkgConfig: { [key: string]: BaseInput | ResourcesInput } = {};
    private orkgPub = getOrkgPub();

    private SOFTWARETEMPLATEID = "R166722"

    public static getInstance(): OrkgAgent {
        if (!OrkgAgent.instance)
            OrkgAgent.instance = new OrkgAgent();
        return OrkgAgent.instance;
    }

    /**
     * Initialize the ORKG Config
     * @remarks THIS NEEDS AN URGENT CHANGE
     * @param repository Github Repository
     * @returns 
     */
    public async initOrkgConfig(repository: Repository) {
        // Create the base ORKG Config
        let templateStatements = await this.getStatementsBundleNew(this.SOFTWARETEMPLATEID, "maxLevel=2&blacklist=ResearchField");
        let templateComponents = createTemplateComponent(templateStatements);
        let orkgConfig = createOrkgConfig(templateComponents);
        this.orkgConfig = orkgConfig;

        let name = repository.name; // full name

        // Search if the repo already exists - If it exists do not change the modified value
        let possibleResources = await this.getResourceClass("Software", name);
        let resourceId: string | undefined;
        if (typeof possibleResources !== "undefined") {
            if (name.toLowerCase() in possibleResources) {
                resourceId = possibleResources[name.toLowerCase()];
            }

        }
        if (typeof resourceId !== "undefined") {
            setOrkgRepositoryId(resourceId);
            // Get the existing statements bundle
            let statements = await this.getStatementsBundle(resourceId, 'maxLevel=1');
            if (typeof statements !== "undefined") {
                for (let param in this.orkgConfig) {
                    if (!(this.orkgConfig[param].id in statements)) { continue; }
                    this.orkgConfig[param].show = true;
                    this.orkgConfig[param].value = statements[this.orkgConfig[param].id].label;
                    this.orkgConfig[param].instanceId = statements[this.orkgConfig[param].id].id;
                    this.orkgConfig[param].statementId = statements[this.orkgConfig[param].id].statementId;

                    if (this.orkgConfig[param].hasOwnProperty('options')) {
                        const optionsDict = await this.getResourceClass(this.orkgConfig[param].class);
                        if (optionsDict) {
                            this.orkgConfig[param].optionsDict = optionsDict;
                            this.orkgConfig[param].options = Object.keys(optionsDict);
                        }
                    }

                }
                // Mark Betty Reference as not modified -------- (I really don't like this)
                if ("orkgSubmitter" in this.orkgConfig) {
                    this.orkgConfig["orkgSubmitter"].show = false;
                    this.orkgConfig["orkgSubmitter"].modified = false;
                }
                // ------------------------------------------------------------------------
            }

        }

        if (this.orkgConfig.name.instanceId === "") {
            this.orkgConfig.name.value = name;
            this.orkgConfig.name.modified = true
            this.orkgConfig.name.show = true;
        }

        // TODO: maybe change the modified value
        this.orkgConfig.citation.show = true;
        // const citationOptionsDict = await OrkgAgent.getInstance().getResourceClass("Citation");
        const rawCitations = await fetchCitations(get(orkgApiConfig).baseUrl);
        const parsedCitations = parseCitations(rawCitations);
        const citationOptionsDict = parsedCitations.reduce((acc: { [index: string]: string }, entry) => {
            acc[entry.label] = entry.id;
            return acc;
        }, {});
        if (citationOptionsDict) {
            (this.orkgConfig.citation as ResourcesInput).optionsDict = citationOptionsDict;
            (this.orkgConfig.citation as ResourcesInput).options = Object.keys(
                citationOptionsDict
            );
        }

        if (repository.mainPaper !== undefined && this.orkgConfig.citation.instanceId === "") {
            this.orkgConfig.citation.value = `https://doi.org/${repository.mainPaper.doi}`
            this.orkgConfig.citation.modified = true;
        }

        this.orkgConfig['code repository'].show = true;
        if (this.orkgConfig['code repository'].instanceId === "") {
            this.orkgConfig['code repository'].value = repository.url;
            this.orkgConfig['code repository'].modified = true;
        }

        this.orkgConfig.description.show = true;
        if (this.orkgConfig.description.instanceId === "") {
            this.orkgConfig.description.value = repository.description;
            this.orkgConfig.description.modified = true;
        }


        // TODO: maybe change the modified value
        this.orkgConfig['programming language'].show = true;
        // const progLangOptionsDict = await OrkgAgent.getInstance().getResourceClass("C29016");
        const rawLanguages = await fetchLanguages(get(orkgApiConfig).baseUrl);
        const parsedLanguages = parseLanguages(rawLanguages);
        const dict = parsedLanguages.reduce((acc: { [index: string]: string }, entry) => {
            acc[entry.label] = entry.id;
            return acc;
        }, {});
        if (dict) {
            (this.orkgConfig['programming language'] as ResourcesInput).optionsDict = dict;
            (this.orkgConfig['programming language'] as ResourcesInput).options = Object.keys(dict);
        }
        if (this.orkgConfig['programming language'].instanceId === "") {
            this.orkgConfig['programming language'].value = repository.language; //Check if the language name is the same
            this.orkgConfig['programming language'].modified = true;
        }

        this.orkgConfig['reference publication'].show = true;
        if (this.orkgConfig['reference publication'].instanceId === "") {
            if (typeof repository.mainPaper !== "undefined") {
                this.orkgConfig['reference publication'].value = repository.mainPaper.title;
                this.orkgConfig['reference publication'].modified = true
            }
        }

        this.orkgConfig['date created'].show = true;
        if (this.orkgConfig['date created'].instanceId === "") {
            this.orkgConfig['date created'].value = repository.createdAt.toISOString().substring(0, 10);
            this.orkgConfig['date created'].modified = true
        }
        this.orkgConfig['date modified'].show = true;
        if (this.orkgConfig['date modified'].instanceId === "") {
            this.orkgConfig['date modified'].value = repository.updatedAt.toISOString().substring(0, 10);
            this.orkgConfig['date modified'].modified = true
        }
        // The Readme needs to be a link
        this.orkgConfig.readme.show = true;
        if (this.orkgConfig.readme.instanceId === "" && repository.readmeUrl) {
            this.orkgConfig.readme.value = repository.readmeUrl;
            this.orkgConfig.readme.modified = true;
        }

        setOrkgConfig(this.orkgConfig)
        return;
    }

    // TODO: Divide this into multiple functions
    /**
     * Initialize the ORKG Publication Config
     * @remarks THIS NEEDS AN URGENT CHANGE
     * @param repository Github Repository
     * @returns 
     */
    public async initOrkgPub(repository: Repository) {
        this.orkgPub = getOrkgPub();

        // Remove the placeholder
        this.orkgPub.publications.pop();

        // Clear publication map
        clearPublicationMap()

        for (let tmpPublication of repository.publications) {

            // Search if the paper already exists
            const possiblePapers = await this.getResourceClass("Paper", tmpPublication.name);

            let paperId: string | undefined;
            if (typeof possiblePapers !== "undefined") {
                // Check if the label is exactly the same as the publication name
                if (tmpPublication.name.toLowerCase() in possiblePapers) {
                    paperId = possiblePapers[tmpPublication.name.toLowerCase()];
                    setValuePublicationMap(tmpPublication.name, paperId);
                }

            }

            let tmpAuthors: ResourcesInput[] = [];
            for (let author of tmpPublication.authorNames) {
                let tmpAuthor = createAuthor(author)
                // get the options for the author          
                // No idea if the author should be a resource or a literal (ORKG uses both)
                const authorOptionsDict = await this.getResourceClass("Author", author);
                if (authorOptionsDict) {
                    tmpAuthor.optionsDict = authorOptionsDict
                    tmpAuthor.options = Object.keys(
                        tmpAuthor.optionsDict
                    );
                }
                tmpAuthors.push(tmpAuthor);
            }


            // let tmpAuthors: ResourcesInput[] = tmpPublication.authorNames.map(createAuthor);


            let tmpOrkgPub: OrkgSinglePublication = {
                authors: tmpAuthors,
                doi: createLiteralInput('DOI', 'P26', tmpPublication.doi, 'text', 'xsd:string'),
                name: createLiteralInput('Name', '', tmpPublication.name, 'text', 'xsd:string'),
                year: createLiteralInput('Publication year', 'P29', tmpPublication.year ? tmpPublication.year.toString() : '', 'number', 'xsd:string'),
                url: createLiteralInput('Url', 'url', tmpPublication.url ? tmpPublication.url[0] : '', 'url', 'xsd:anyURI'),
                abstract: createLiteralInput('Abstract', 'P37015', tmpPublication.abstract ? tmpPublication.abstract : '', 'text', 'xsd:string'),
            }

            // // get the options for the venue(source)            
            // const venueOptionsDict = await this.getResourceClass("Venue", tmpPublication.source);
            // if (venueOptionsDict) {
            //     tmpOrkgPub.source.optionsDict = venueOptionsDict
            //     tmpOrkgPub.source.options = Object.keys(
            //         tmpOrkgPub.source.optionsDict
            //     );
            // }

            // TODO: Fill the values with the ones from ORKG
            if (typeof paperId !== "undefined") {
                let statements = await this.getStatementsBundle(paperId);

                if (typeof statements !== "undefined") {

                    for (let param in tmpOrkgPub) {
                        if (!(tmpOrkgPub[param as keyof OrkgSinglePublication].id in statements)) { continue; }

                        // TODO: Handle authors
                        if (param === "authors") {
                            // for (let author of tmpOrkgPub.authors) {}
                            continue;
                        }

                        tmpOrkgPub[param as keyof OrkgSinglePublication].value = statements[tmpOrkgPub[param as keyof OrkgSinglePublication].id].label;
                        tmpOrkgPub[param as keyof OrkgSinglePublication].instanceId = statements[tmpOrkgPub[param as keyof OrkgSinglePublication].id].id;

                    }
                }
            }


            this.orkgPub.publications.push(tmpOrkgPub)
        }

        setOrkgPub(this.orkgPub);
        return;

    }

    /**
     * Submits the repository to ORKG
     * based on the ORKG config
     */
    public async submitRepository() {
        this.orkgConfig = getOrkgConfig();
        // 1. Create Repository if it doesn't exists and get the id
        let repositoryId = ""
        if (getOrkgRepositoryId() !== "") {
            repositoryId = getOrkgRepositoryId()
        } else {
            repositoryId = await this.createResource(this.orkgConfig.name.value, "Software");
            setOrkgRepositoryId(repositoryId);
        }
        // 2. For each parameter, create the literal or resource and get the ids 
        for (let param in this.orkgConfig) {
            let elementId: string;
            let predicateID: string = this.orkgConfig[param].id;
            // TODO: edit all the "param" into a single variable
            if (this.orkgConfig[param].delete) {
                if (this.orkgConfig[param].statementId !== '') {
                    this.orkgApi.deleteStatement(this.orkgConfig[param].statementId);
                }
                continue;
            }
            // 2.1 If the literal/resource already exists, edit the value
            if (!this.orkgConfig[param].modified) {
                continue;
            }
            if (this.orkgConfig[param].nodeType === NodeType.literals) {
                if (this.orkgConfig[param].instanceId !== "") {
                    // Edit the element
                    elementId = await this.editLiteral(this.orkgConfig[param].value, this.orkgConfig[param].dataType, this.orkgConfig[param].instanceId);
                    // elementId = this.orkgConfig[param].instanceId; // This would also work
                    continue; // No need to create a new statement
                } else {
                    elementId = await this.createLiteral(this.orkgConfig[param].value, this.orkgConfig[param].dataType)
                }
            } else if (this.orkgConfig[param].nodeType === NodeType.resources) {
                // If the selected resource already exists
                if (this.orkgConfig[param].value in this.orkgConfig[param].optionsDict) {
                    elementId = this.orkgConfig[param].optionsDict[this.orkgConfig[param].value]
                } else {
                    elementId = await this.createResource(this.orkgConfig[param].value, this.orkgConfig[param].class);
                }
            } else { continue; }
            // 3. Create the statement if the literal/resource doesn't exists 
            this.orkgApi.createStatement(repositoryId, predicateID, elementId);

        }
        notificationStore.set({
            type: NotificationType.success,
            title: "Success",
            message: `The Repository information has been submited to ORKG <br><a href=${get(orkgApiConfig).baseUrl}/resource/${repositoryId} rel="noopener" target="_blank">${get(orkgApiVersion) === "normal" ? "ORKG" : "Sandbox"} Link</a>`,
        });
    }

    // TODO: divide this function into multiple functions
    /**
     * Submits the publications to ORKG
     * based on the ORKG Publications config
     */
    public async submitPublications() {
        this.orkgPub = getOrkgPub();
        const contributionPredicate = "P31"
        const NFDI4ING_CONTRIB = 'NFDI4Ing Contribution'
        const repositoryId = getOrkgRepositoryId()
        for (let publication of this.orkgPub.publications) {
            // 1. Create the Paper if paper doesnt exist else get resourceid
            let paperId;
            if (hasValuePublicationMap(publication.name.value)) {
                paperId = getValuePublicationMap(publication.name.value);
            } else {
                paperId = await this.createResource(publication.name.value, "Paper");
            }

            // 2. Create contribution if contribution doesnt exist else get id
            let paperStatements = await this.getStatementsBundleNew(paperId);
            let contributionId;
            if (typeof paperStatements !== "undefined") {
                for (let stm of paperStatements) {
                    if (stm.object.label === NFDI4ING_CONTRIB) {
                        contributionId = stm.object.id;
                        break;
                    }
                }
            }
            if (typeof contributionId === "undefined") {
                contributionId = await this.createResource(NFDI4ING_CONTRIB, 'Contribution');
            }
            this.orkgApi.createStatement(paperId, contributionPredicate, contributionId);

            // 3. Add properties to Paper
            for (let param in publication) {
                let predicateId = '';
                // Handle first the authors or else they will break everything
                if (param === "authors") {
                    for (let author of publication.authors) {
                        if (!author.modified) {
                            continue;
                        }
                        let authorId: string;
                        predicateId = author.id;
                        if (author.value in author.optionsDict) {
                            authorId = author.optionsDict[author.value];
                        } else {
                            authorId = await this.createLiteral(author.value, author.dataType)
                        }
                        // Have to create the statements for the authors here or else they will break everything
                        this.orkgApi.createStatement(paperId, predicateId, authorId);
                    }
                    continue;
                }
                //Everything but the authors
                let singleInput = publication[param as keyof OrkgSinglePublication];
                let elementId: string;
                predicateId = singleInput.id;

                // Clean if its a doi
                if (param === "doi") {
                    singleInput.value = getDoi(singleInput.value);
                }
                if (!singleInput.modified || singleInput.value === "") {
                    continue;
                }
                if (singleInput.nodeType === NodeType.literals) {
                    if (singleInput.instanceId !== "") {
                        // Edit the element
                        elementId = await this.editLiteral(singleInput.value, singleInput.dataType, singleInput.instanceId);
                        // elementId = this.orkgConfig[param].instanceId; // This would also work
                        continue; // No need to create a new statement
                    } else {
                        elementId = await this.createLiteral(singleInput.value, singleInput.dataType)
                    }
                } else if (singleInput.nodeType === NodeType.resources) {
                    // If the selected resource already exists
                    if (singleInput.value in singleInput.optionsDict) {
                        elementId = singleInput.optionsDict[singleInput.value]
                    } else {
                        elementId = await this.createResource(singleInput.value, singleInput.class);
                    }
                } else { continue; }
                // 3.2 Create statements
                this.orkgApi.createStatement(paperId, predicateId, elementId);
            }
            // 4. Add properties to contribution: "cites repository" if it doesn't exist
            let contributionStatements = await this.getStatementsBundleNew(contributionId);
            let contributionExists = false;
            if (typeof contributionStatements !== "undefined") {
                for (let stm of contributionStatements) {
                    if (stm.object.id === repositoryId) {
                        contributionExists = true;
                        break;
                    }
                }
            }
            if (!contributionExists) {
                this.orkgApi.createStatement(paperId, contributionPredicate, contributionId);
            }

            // Create cite repository statement:
            this.orkgApi.createStatement(contributionId, get(orkgApiConfig).citesDoiProperty, repositoryId);
            // Create abstract 
            if (publication.abstract.value !== "") {
                let abstractId = await this.createLiteral(publication.abstract.value, publication.abstract.dataType);
                this.orkgApi.createStatement(contributionId, publication.abstract.id, abstractId);
            }
        }
    }

    /**
     * Get the user authentication token
     * @param username username
     * @param password password
     * @returns true if token was set, false otherwise
     */
    public async getAuthToken(username: string, password: string): Promise<string> {
        try {
            let json = await this.orkgApi.callAuth(username, password, HttpRequestsMethods.POST);
            // setAuthTokenOrkg(json.access_token);
            return json.access_token;
            // return true;
        } catch (error) {
            // TODO: send back some error to the front end 
            // return false;
            return "";
        }
    }

    /**
     * Get the resources for a class. 
     * Optional search for specific resources of the class
     * @param classId Class
     * @param queryString Optional: Query
     * @returns Object with resources[resource.label] = resource.id
     */
    public async getResourceClass(classId: string, queryString?: string) {
        try {
            let json = await this.orkgApi.searchResourceClass(classId, `q=${queryString}`);
            let resources: { [key: string]: string } = {};
            json.content.forEach((resource: { label: string, id: string }) => {
                resources[resource.label.toLowerCase()] = resource.id;
            });
            return resources;
        } catch (error) {
            //TODO:
            console.error(error)
        }
    }

    /**
     * Creates a resource 
     * @param label Resource label
     * @param _class Resource class
     * @returns resource id
     */
    private async createResource(label: string, _class: string) {
        try {
            let resource = await this.orkgApi.createResource(label, _class);
            return resource.id;
        } catch (error) {
            // TODO:
            console.error(error)
        }
    }

    /**
     * Creates a literal
     * @param label Literal label
     * @param datatype Literal datatype
     * @returns literal id
     */
    private async createLiteral(label: string, datatype: string) {
        try {
            let literal = await this.orkgApi.createLiteral(label, datatype);
            return literal.id;
        } catch (error) {
            // TODO:
            console.error(error)
        }
    }

    /**
     * Edits an existing literal    
     * @param label Literal label
     * @param datatype Literal Datatype
     * @param literalId Literal ID
     * @returns Literal ID
     */
    private async editLiteral(label: string, datatype: string, literalId: string) {
        try {
            let literal = await this.orkgApi.editLiteral(label, datatype, literalId);
            return literal.id;
        } catch (error) {
            // TODO:
            console.error(error)
        }
    }

    /**
     * Get all the statements from a resource in a keyed object
     * @param resourceId Resource Id
     * @returns Object with statements[predicateId] = {id: objectId,label: objectLabel}
     */
    private async getStatementsBundle(resourceId: string, queryString?: string) {
        try {
            let json = await this.orkgApi.getStatementBundle(resourceId, queryString);
            let statements: { [key: string]: { id: string, label: string, statementId: string } } = {};

            const jsonStatements = json.statements;
            for (const statement of jsonStatements) {
                const predicateId = statement.predicate.id;
                const objectId = statement.object.id;
                const objectLabel = statement.object.label;
                const statementId = statement.id;

                statements[predicateId] = {
                    id: objectId,
                    label: objectLabel,
                    statementId: statementId
                }
            }
            return statements;
        } catch (error) {
            // TODO:
            console.error(error)
        }
    }

    // TODO: CHANGE NAME
    /**
     * Returns a list of statements from a resource
     * @param resourceId Resource id
     * @param queryString Query string to filter statements. eg. maxLevel=2&blacklist=ResearchFild
     * @returns List of statements
     */
    private async getStatementsBundleNew(resourceId: string, queryString?: string) {
        try {
            let json = await this.orkgApi.getStatementBundle(resourceId, queryString);
            let statements: Statement[] = [];

            const jsonStatements = json.statements;
            for (const statement of jsonStatements) {
                statements.push(parseJsonStatement(statement))
            }
            return statements;
        } catch (error) {
            // TODO:
            console.error(error)
        }
    }

}

