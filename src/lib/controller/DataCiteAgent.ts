import DataCiteAPI from "$lib/api/datacite.api";
import { githubResults } from "$lib/stores/githubResults.store";
import type { Repository } from "$lib/types/githubResults.type";
import ZenodoUtil from "$lib/util/zenodo.util";

export class DataCiteAgent {
  id: number;
  static maxID: number = 0;

  constructor() {
    this.id = DataCiteAgent.maxID;
    DataCiteAgent.maxID++;
  }

  /**
   * get the Doi and then get all Citations
   * @param repo Repository
   */
  async startDataCiteSearch(repo: Repository) {
    // Only make the call if the doi was previosly found -- Maybe change this
    // Doing in this way to save api calls
    if (
      repo.mainPaper === undefined ||
      repo.mainPaper.citationsArray === undefined
    ) {
      return;
    }

    try {
      // we loop over each citation which are defined in citationsArray
      for (const citationDoi of repo.mainPaper.citationsArray) {
        try {
          const pub = await DataCiteAPI.getCitations(citationDoi);
          githubResults.addPublication(repo.name, pub);
        } catch (error) {
          // When no citations was found
          // console.log(error);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Gets the amount of zenodo citations or other citations
   * @param repo Repository
   */
  async getAmountCitations(repo: Repository) {
    let zenodoDoi = "error";
    let doiFromZenodo = false;
    let url = "";
    if (repo.mainPaper !== undefined) {
      if (repo.mainPaper.doi !== undefined) {
        zenodoDoi = repo.mainPaper.doi;
        url = "https://api.datacite.org/dois/" + zenodoDoi;
      }
    } else {
      zenodoDoi = ZenodoUtil.getZenodoDoi(repo.readme);
      doiFromZenodo = true;
      url = "https://api.datacite.org/dois/10.5281/zenodo." + zenodoDoi;
    }

    // Only make the call to Datacite if the doi was found
    if (zenodoDoi == "error") {
      githubResults.setAmountPublications(repo.name, "DataCite", 0);
      return;
    }

    try {
      let mainPaper = await DataCiteAPI.getMainPaper(url);

      if (typeof repo.mainPaper !== "undefined") {
        mainPaper = {
          doi: repo.mainPaper.doi ?? mainPaper.doi,
          title: repo.mainPaper.title ?? mainPaper.title,
          journal: repo.mainPaper.journal ?? mainPaper.journal,
          dateReleased: mainPaper.dateReleased,
          abstract: repo.mainPaper.abstract,
          citationsArray: mainPaper.citationsArray,
        };
      }

      // Update Repo Main Paper
      repo.mainPaper = mainPaper;
      // Update the agent Query term of the repo
      if (doiFromZenodo) {
        zenodoDoi = "10.5281/zenodo." + zenodoDoi;
      }
      repo.agentQueryTerm.dataCite = zenodoDoi;

      githubResults.updateRepo(repo);

      if (mainPaper.citationsArray !== undefined) {
        githubResults.setAmountPublications(
          repo.name,
          "DataCite",
          mainPaper.citationsArray?.length
        );
      }
    } catch (error) {
      // console.log(error)
      githubResults.setAmountPublications(repo.name, "DataCite", 0);
    }
  }
}
