import { githubResults } from '$lib/stores/githubResults.store';
import type { Search, Repository, Publication, MainPaper, AgentQueryTerm } from '$lib/types/githubResults.type';
import yaml from 'js-yaml';
import { authTokenGithub } from '$lib/stores/githubResults.store';
import { endpointMetaInfo } from '$lib/stores/mainPaperInfo.store';
import { get } from "svelte/store";
import Manager from './Manager';
import { notificationStore } from '$lib/stores/clientNotification.store';
import { NotificationType } from '$lib/types/clientNotification.type';
import { GitHubAPI } from '$lib/api/github.api';


export class githubAgent {
    id: number;
    static maxID: number = 0;
    stop: boolean;

    /**
     * creates a Github Agent
     * @param search the Query String
     * @param jobs a job list containing page numbers of Github
     * @param id the id Of the Agent
     */
    // old constructor
    /*constructor(search: string, jobs: number[], id: number) {
        this.search = search;
        this.jobs = jobs;
        this.id = id;
    }*/

    constructor() {
        this.id = githubAgent.maxID;
        githubAgent.maxID++;
        this.stop = false;

    }

    /**
     * makes a quick Peek with filter/queryLimit on Github if simpleSearch == True for the Search Query and stores the Search Object with the total amount of repos
     * in the Store
     * @param q query String
     * @param queryLimit search filter String
     */
    async startPeek(q: string) {
        const queryString = '?q=' + encodeURIComponent(`${q}`);
        const url = 'https://api.github.com/search/repositories' + queryString;
        try {
            let serializedResult
            const res = await fetch(url + '&page=' + "1" + '&size=100', {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'token ' + get(authTokenGithub).valueOf()
                }
            })
            if (res.status === 401)
                notificationStore.set({ type: NotificationType.error, title: "Github Error", message: "Please enter a valid Github token in the settings first." })
            serializedResult = await res.json()
            let search: Search = {
                query: q,
                totalRepos: serializedResult.total_count,
                repositories: new Map() as Map<string, Repository>
            }
            githubResults.setSearch(search)
            endpointMetaInfo.reset() // Reset the metainfo 
        } catch (e) {
            console.error(e);
        }
    };
    /**
     * Spaguetti Code :(
     * @param q Query
     * @param queryExtension Query Endpoint (Code or repository)
     * @param queryLimit delimiter for the query
     * @returns Amount of repos found
     */
    async getAmountRepos(q: string, queryExtension: string, queryLimit: string) {
        const queryString = '?q=' + encodeURIComponent(`${q}` + ' ' + queryLimit);
        const url = 'https://api.github.com/search/' + queryExtension + queryString;
        try {
            const res = await fetch(url + '&page=' + "1" + '&size=100', {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'token ' + get(authTokenGithub).valueOf()
                }
            })
            const responseJson = await res.json();
            return responseJson.total_count;

        } catch (e) {
            console.error(e);
            return 0;
        }

    };



    /**
     * iterates over all pageNumbers in the Job list saved in the Agent Object, which runs this method
     * iterates over all repos in a page and stores a Repo Object in the Store for each one.
     * @param q the Query String
     * @param pageIndex the page numbers to be extracted by the GithubAgent
     * @param reposPerPage the amount of repos per taken page. Maximum is 100
     */
    async startSearch(q: string, pageIndex: number[], reposPerPage: number, queryExtension: string) {
        const queryString = '?q=' + encodeURIComponent(`${q}`);
        const url = 'https://api.github.com/search/' + queryExtension + queryString;
        try {
            for (const pageNumber of pageIndex) {
                // Stop mechanism
                if (this.stop) {
                    break;
                }

                let res = await fetch(url + '&page=' + pageNumber + '&per_page=' + reposPerPage, {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'token ' + get(authTokenGithub).valueOf()
                    }
                })
                // console.log((url + '&page=' + pageNumber + '&per_page=' + reposPerPage))
                let json = await res.json();
                for (let i = 0; i < json.items.length; i++) {
                    // Stop mechanism
                    if (this.stop) {
                        break;
                    }

                    // Repo Info
                    let repositoryInfo = json.items[i];

                    // When the endpoint is different
                    if (queryExtension === 'code') { repositoryInfo = json.items[i].repository }

                    // Temporary amount of publicatons hash map
                    let tmpAmountPub = new Map() as Map<string, number>;
                    // Start the values at 0 to solve problems while sorting
                    tmpAmountPub.set('zenodo', 0);
                    tmpAmountPub.set('citationCff', 0);

                    // If there is a doi in the description, get it
                    let descriptionDoi = this.getDoi(repositoryInfo.description)
                    let mainPaper: MainPaper | undefined;
                    if (descriptionDoi !== "") {
                        // Create a MainPaper
                        mainPaper = {
                            doi: descriptionDoi,
                            title: undefined,
                            journal: undefined,
                            dateReleased: undefined,
                            abstract: undefined,
                            citationsArray: undefined
                        }

                    }
                    // get files in repository
                    const files = await GitHubAPI.getInstance().getFiles(repositoryInfo.full_name);

                    // get citation.cff
                    // let citation = await this.getCitationCffStart(repositoryInfo.full_name)
                    let citation = await GitHubAPI.getInstance().getCitationCFFFromFiles(files)
                    let citationPub: Publication | undefined;
                    if (citation !== 'nothing found') {
                        let tmpMainPaper = this.parseCitationCff(citation)
                        if (tmpMainPaper !== undefined) {
                            mainPaper = tmpMainPaper;
                        }
                    }

                    if (mainPaper !== undefined) {
                        mainPaper = await this.getMainPaperFromDoi(mainPaper);
                        citationPub = this.createMainPaperPublication(mainPaper);
                        tmpAmountPub.set('citationCff', 1);
                    }


                    // For some reason some repos are "undefined"
                    let readme: string = '';
                    let readmeUrl: string | undefined;
                    try {
                        [readme, readmeUrl] = await GitHubAPI.getInstance().getReadmeFromFiles(files, repositoryInfo.full_name);
                        // readme = await this.getReadmeStart(repositoryInfo.full_name).then(res => res).then(out => { return out })
                    } catch (e) {

                    }

                    // The query terms used by the agents
                    let agentQuery: AgentQueryTerm = {
                        zenodo: '',
                        openAlex: '',
                        openCitations: '',
                        dataCite: ''
                    }

                    let repo: Repository = {
                        name: repositoryInfo.full_name,
                        url: repositoryInfo.html_url,
                        stars: repositoryInfo.stargazers_count,
                        forks: repositoryInfo.forks_count,
                        readme: readme,
                        readmeUrl: readmeUrl,
                        publications: new Array() as Array<Publication>,
                        createdAt: new Date(repositoryInfo.created_at),
                        updatedAt: new Date(repositoryInfo.updated_at),
                        language: repositoryInfo.language,
                        homepage: repositoryInfo.homepage,
                        mainPaper: mainPaper,
                        amountPublications: tmpAmountPub,
                        gotOpenCitationsPub: false,
                        gotDataCitePub: false,
                        agentQueryTerm: agentQuery,
                        description: repositoryInfo.description
                    };
                    // console.log(repo);
                    githubResults.addRepo(repo);
                    if (citationPub !== undefined) {
                        githubResults.addPublication(repo.name, citationPub);
                    }
                }
                Manager.statusGithubAgent(this.id, true, pageNumber);

            }
            Manager.statusGithubAgent(this.id, false, -1);
        } catch (e) {
            console.log(`Error ${e}`);
            Manager.statusGithubAgent(this.id, false, -1);
        }
    }



    /**
     * If the readme is found then the readme function returns the readme,
     * it will return the readme otherwise it will try again to a max of 3 trys
     * @param repoName The Github repo name example: tensorflow/tensorflow
     * @returns a 404 Not Found error or the found readme
     */
    async getReadmeStart(repoName: string): Promise<[string, string]> {

        let [readme, readmeFileName] = await this.getReadme(repoName);
        if (readme !== 'Null') {
            return [readme, readmeFileName];
        }


        let response = await fetch("https://api.github.com/repos/" + repoName + "/readme")
        if (response.status === 200) {
            let json = await response.json()
            readme = atob(json.content)
            readmeFileName = json.name;
        }

        return [readme, readmeFileName];
    }

    /**
     * Start the search for a citation.cff file
     * @param repoName Name of the repository
     * @returns Citation File or nothing found
     */
    async getCitationCffStart(repoName: string) {
        let maxTrys = 0;
        while (maxTrys < 1) {
            let citation_cff = await this.getCitationCff(repoName)
            if (citation_cff !== "404: Not Found") {
                // console.log("+-+-+-+-+-+-Citation Found+-+-+-+-+-+-")
                // console.log(citation_cff)
                return citation_cff
            }
            maxTrys += 1;
        }
        return 'nothing found'
    }



    /**
     * If the readme is found then the function instantly terminates and returns the readme,
     * if it doesnt find it returns 404 Not Found
     * @param full_name The Github repo name example: tensorflow/tensorflow
     * @returns a 404 Not Found error or the found readme
     */
    async getReadme(full_name: String): Promise<[string, string]> {
        const url = 'https://raw.githubusercontent.com/' + full_name + '/master/';
        const readmeArr = ["README.md", "README.rst", "readme.md"]
        // the purpose is to do readme requests sequentially, to avoid unnecessary requests
        for (const readmeType of readmeArr) {
            const responseReadme = await fetch(url + readmeType);
            if (responseReadme.ok) {
                return [await responseReadme.text(), readmeType];
            }
        }
        return ['Null', ''];
    }

    /**
     * Makes a call to raw.github to get the citation file
     * @param full_name Full Repository Name
     * @returns The CITATION.CFF file or not found
     */
    async getCitationCff(full_name: String) {
        const url = 'https://raw.githubusercontent.com/' + full_name + '/master/CITATION.cff';
        try {
            const res = await fetch(url)
            return res.text()
        }
        catch (e) {
            return '404: Not Found'

        }
    }

    /**
     * Use Yaml to parse the CITATION.CFF file
     * @param citationCff Citation cff file as string
     * @returns Citation Object or undefined
     */
    parseCitationCff(citationCff: string) {
        try {
            let result = yaml.load(citationCff)

            let title = result.title;

            if (result['preferred-citation'] !== undefined) {
                if (result['preferred-citation'].title !== undefined) {
                    title = result['preferred-citation'].title;
                }
            }

            let citation: MainPaper = {
                doi: result.doi !== undefined ? this.getDoi(result.doi) : '',
                title: title,
                journal: result.journal,
                // authorNames: 
                dateReleased: new Date(result['date-released']),
                abstract: result.abstract,
                citationsArray: undefined
            }
            return citation
        } catch (error) {
            console.log("YAML ERROR");
            return undefined;
        }
    }

    private async getMainPaperFromDoi(mainPaper: MainPaper) {
        let doi = mainPaper.doi;
        if (doi === "") return mainPaper;
        // The domain changes depending on where is ran
        let url = `${document.location.origin}/api/extract-paper-from-url?url=https://doi.org/${doi}`;
        let page = await fetch(url);
        if (!page.ok) return mainPaper;
        let jsonPaper = await page.json();
        mainPaper.title = mainPaper.title ?? jsonPaper["TITLE"];
        mainPaper.journal = mainPaper.journal ?? jsonPaper["JOURNAL"];
        let tmpDate = new Date()
        tmpDate.setFullYear(jsonPaper["YEAR"])
        mainPaper.dateReleased = (mainPaper.dateReleased instanceof Date && !isNaN(mainPaper.dateReleased.getTime())) ? mainPaper.dateReleased : tmpDate;
        mainPaper.abstract = mainPaper.abstract ?? jsonPaper["ABSTRACT"];

        if (jsonPaper["ABSTRACT"] !== "") { endpointMetaInfo.incrementAbstract(); }

        endpointMetaInfo.incrementTotal()

        return mainPaper;

    }

    private createMainPaperPublication(mainPaper: MainPaper) {
        return {
            doi: mainPaper?.doi,
            name: mainPaper?.title,
            source: '',
            authorNames: [],
            externalIds: undefined,
            abstract: mainPaper?.abstract,
            venue: undefined,
            year: undefined,
            referenceCount: undefined,
            citationCount: undefined,
            fieldsOfStudy: undefined,
            publicationTypes: undefined,
            publicationDate: mainPaper?.dateReleased,
            journal: mainPaper?.journal,
            url: undefined
        }
    }

    /**
     * Gets the doi from a text string
     * @param text text string
     * @returns doi if found
     */
    private getDoi(text: string) {
        try {
            let doiIdPattern = /10\.\d{4,9}\/[-._()/:A-Za-z0-9]+/;

            // Temp Doi
            var doi = "";

            let match1 = doiIdPattern.exec(text);
            if (match1 !== null) {
                doi = match1[0];
            }

            return doi;
        } catch (e) {
            console.log(`Error ${e}`);
            return "";
        }
    }

    /**
     * Sets the stop variable
     * @param value Value to set the stop variable
     */
    setStop(value: boolean) {
        this.stop = value;
        console.log(`Github Agent ${this.id} set to ${this.stop}`);
    }

}