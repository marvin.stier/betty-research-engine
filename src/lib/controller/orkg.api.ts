import { orkgApiConfig } from "$lib/stores/authTokens.store";
import { get } from 'svelte/store'
export enum HttpRequestsMethods {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE"
}

/**
 * ORKG Node types
 */
export enum NodeType {
    statements = "statements",
    resources = "resources",
    literals = "literals",
    predicates = "predicates",
    classes = "classes"
}

export class OrkgAPI {
    private static instance: OrkgAPI;

    public static getInstance(): OrkgAPI {
        if (!OrkgAPI.instance)
            OrkgAPI.instance = new OrkgAPI();
        return OrkgAPI.instance;
    }

    // TODO: Change the payload type
    /**
     * Make a call to the ORKG API
     * @param nodeType Resource, Literal or Statement
     * @param payload data to be sent
     * @param requestMethod HTTP request method
     * @param query optional query term 
     * @param nodeId optional node id
     * @param classId optional class id
     * @returns Response Json
     */
    private async callApi(
        nodeType: NodeType,
        payload: any,
        requestMethod: HttpRequestsMethods,
        query?: string,
        nodeId?: string,
        classId?: string
    )
        : Promise<any> {
        const apiConfig = get(orkgApiConfig);
        const BASE_URL = apiConfig.baseUrl;
        const authToken = apiConfig.accessToken;
        let url = `${BASE_URL}/api/${nodeType}/`
        if (typeof nodeId !== "undefined") { url += `${nodeId}/`; }
        else if (typeof classId !== "undefined") { url = `${BASE_URL}/api/${NodeType.classes}/${classId}/${nodeType}/`; }
        if (typeof query !== "undefined") { url += `?${query}`; }

        let options: {
            method: string;
            headers: {
                'Content-Type': string;
                'authorization'?: string;
            };
            body?: string;
        } = {
            method: requestMethod,
            headers: {
                'Content-Type': "application/json",
                'authorization': `Bearer ${authToken}`
            },
            body: JSON.stringify(payload)
        }
        if (requestMethod === HttpRequestsMethods.GET) {
            delete options.body;
            delete options.headers.authorization;
        }

        let res = await fetch(url, options);

        if (!res.ok) {
            return Promise.reject('error calling the ORKG API')
        }
        const json = await res.json();
        return json;
    }

    /**
     * Make a call to the ORKG auth api
     * @param username username
     * @param password password
     * @param requestMethod HTTP request method
     * @returns Response Json
     */
    public async callAuth(
        username: string,
        password: string,
        requestMethod: HttpRequestsMethods
    )
        : Promise<any> {
        const BASE_URL = get(orkgApiConfig).baseUrl;
        let url = `${BASE_URL}/oauth/token`

        const options = {
            method: requestMethod,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'authorization': `Basic ${window.btoa('orkg-client:secret')}`
            },
            body: new URLSearchParams({
                grant_type: 'password',
                username: username,
                password: password
            })
        }

        let res = await fetch(url, options);

        if (!res.ok) {
            return Promise.reject('error calling the ORKG API')
        }
        const json = await res.json();
        return json;
    }

    /**
     * Create a resource
     * @param label Name of the resource
     * @param _class Class of the resource
     * @returns Response Json
     */
    public async createResource(label: string, _class: string) {
        const payload = {
            "label": label,
            "classes": [_class]
        }
        return this.callApi(NodeType.resources, payload, HttpRequestsMethods.POST);
    }

    /**
     * Create a literal
     * @param label Literal name
     * @param datatype Literal datatype eg: "xsd:anyURI", "xsd:string"
     * @returns Response Json
     */
    public async createLiteral(label: string, datatype: string) {
        const payload = {
            "label": label,
            "datatype": datatype
        }
        return this.callApi(NodeType.literals, payload, HttpRequestsMethods.POST);
    }

    /**
     * Create a statement
     * @param subject Statement Subject. Normaly the main node
     * @param predicate Statement predicate. 
     * @param object Statement object. The object connected to the subject
     * @returns Response Json
     */
    public async createStatement(subject: string, predicate: string, object: string) {
        const payload = {
            "subject_id": subject,
            "predicate_id": predicate,
            "object_id": object
        }
        return this.callApi(NodeType.statements, payload, HttpRequestsMethods.POST);
    }

    public async deleteStatement(statementId: string) {
        return this.callApi(NodeType.statements, {}, HttpRequestsMethods.DELETE, undefined, statementId)
    }

    /**
     * Edit an existing literal
     * @param label Literal label
     * @param datatype Literal datatype. eg: "xsd:anyURI", "xsd:string"
     * @param literalId Literal Id      
     * @returns Response Json
     */
    public async editLiteral(label: string, datatype: string, literalId: string) {
        const payload = {
            "label": label,
            "datatype": datatype
        }
        return this.callApi(NodeType.literals, payload, HttpRequestsMethods.PUT, undefined, literalId, undefined);
    }

    /**
     * Search a resource    
     * @param queryString Search query
     * @returns Response Json
     */
    public async searchResource(queryString: string) {
        return this.callApi(NodeType.resources, {}, HttpRequestsMethods.GET, queryString);
    }

    /**
     * Search for a resource of a specific class
     * @param classId Resource class id
     * @param queryString Search Query
     * @returns Response Json
     */
    public async searchResourceClass(classId: string, queryString?: string) {
        return this.callApi(NodeType.resources, {}, HttpRequestsMethods.GET, queryString, undefined, classId);
    }

    /**
     * Get the statements from a resource
     * @param resourceId Resource Id
     * @returns Response Json
     */
    public async getStatementBundle(resourceId: string, queryString?: string) {
        // I don't really like how the nodeId is defined
        return this.callApi(NodeType.statements, {}, HttpRequestsMethods.GET, queryString, `${resourceId}/bundle`, undefined);
    }

}