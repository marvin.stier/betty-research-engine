abstract class StatementElement {
    id: string;
    label: string;

    constructor(id: string, label: string) {
        this.id = id;
        this.label = label;
    }

}
/**
 * ORKG Resource
 */
class Resource extends StatementElement {
    classes: string[];
    constructor(id: string, label: string, classes: string[]) {
        super(id, label);
        this.classes = classes
    }
}

/**
 * ORKG Literal
 */
class Literal extends StatementElement {
    datatype: string;
    constructor(id: string, label: string, datatype: string) {
        super(id, label);
        this.datatype = datatype;
    }
}

/**
 * ORKG Predicate
 */
class Predicate extends StatementElement {
    description?: string;
    constructor(id: string, label: string, description?: string) {
        super(id, label);
        this.description = description;
    }
}

/**
 * ORKG Class
 * @remarks This are XMLSchema classes like String,Integer or Date
 * only used in templates
 */
class _Class extends StatementElement {
    uri: string;
    description?: string;
    constructor(id: string, label: string, uri: string, description?: string) {
        super(id, label);
        this.uri = uri;
        this.description = description;
    }
}

/**
 * ORKG Statement
 */
export class Statement {
    id: string;
    subject: Resource | Literal | _Class | Predicate;
    predicate: Predicate;
    object: Resource | Literal | _Class | Predicate;
    constructor(id: string, subject: Resource | Literal | _Class | Predicate,
        predicate: Predicate, object: Resource | Literal | _Class | Predicate) {
        this.id = id;
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;

    }
}

// TODO: move this to utils or something
/**
 * Create a Statement object from a json
 * @param statementJson Statement in json form
 * @returns Statement object
 */
export function parseJsonStatement(statementJson: any) {
    let subject: Resource | Literal | _Class | Predicate;
    let predicate: Predicate;
    let object: Resource | Literal | _Class | Predicate;
    // Create the subject
    subject = parseJsonResourceLiteral(statementJson.subject);

    // Create the object
    object = parseJsonResourceLiteral(statementJson.object);

    // Create the predicate
    let tmpPredicate = statementJson.predicate;
    predicate = new Predicate(tmpPredicate["id"],
        tmpPredicate["label"], tmpPredicate["description"]);

    return new Statement(statementJson["id"], subject, predicate, object)

}

/**
 * Create a resource or literal object from json
 * @param elementJson Resource or literal json
 * @returns Resource or literal objects
 */
function parseJsonResourceLiteral(elementJson: any) {
    let element: Resource | Literal | _Class | Predicate;
    if (elementJson["_class"] == "resource") {
        element = new Resource(elementJson["id"],
            elementJson["label"],
            elementJson["classes"])
    } else if (elementJson["_class"] == "literal") {
        element = new Literal(elementJson["id"],
            elementJson["label"],
            elementJson["datatype"])
    } else if (elementJson["_class"] == "class") {
        element = new _Class(elementJson["id"],
            elementJson["label"],
            elementJson["uri"],
            elementJson["description"])
    } else if (elementJson["_class"] == "predicate") {
        element = new Predicate(elementJson["id"],
            elementJson["label"],
            elementJson["description"])
    } else {
        throw new Error(`Non supported subject or object class ${elementJson["_class"]}`)
    }
    return element;
}

// TODO: The template components
// I basically need to filter the statements that build a template component
// and build a component from that
// tip: search statements which the subject has the "PropertyShape" class
// + then only have the template components (this would be done in another function)
// https://sandbox.orkg.org/resource/R250931
// https://sandbox.orkg.org/resource/R250930
export function createTemplateComponent(statementArray: Statement[]) {

    // Filter all the components
    let tmpStmArray = statementArray.filter((stm) => {
        if (stm.subject instanceof Resource) {
            return stm.subject.classes[0] === "PropertyShape"
        }
        return false
    })

    // Create the components
    let componentsMap = new Map<string, any>;

    for (let tmpStm of tmpStmArray) {
        let tmpId = tmpStm.subject.id;
        let tmpObject: { [k: string]: any } = {}
        let keyLabel = tmpStm.predicate.label;
        let object = tmpStm.object
        if (componentsMap.has(tmpId)) {
            tmpObject = componentsMap.get(tmpId);
        }
        tmpObject[keyLabel] = object
        componentsMap.set(tmpId, tmpObject);
    }


    return componentsMap
}