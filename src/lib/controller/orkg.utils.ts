import type { BaseInput, ResourcesInput } from "$lib/types/orkg.type";
import { NodeType } from "./orkg.api";
import { orkgApiConfig } from "$lib/stores/authTokens.store";
import { get } from "svelte/store";



/**
 * Create an literal object to be saved for the publications
 * @param name Name of the input
 * @param id Orkg ID of the input predicate
 * @param inputType Input Type
 * @param datatype data type
 */
export function createLiteralInput(name: string, id: string, value: string, inputType: string, datatype: string, modified: boolean = true): BaseInput {
    return {
        value: value,
        id: id,
        instanceId: '',
        inputType: inputType,
        nodeType: NodeType.literals,
        name: name,
        dataType: datatype,
        class: '',
        statementId: '',
        modified: modified,
        show: false,
        delete: false
    }
}

/**
 * Create an resource object to be saved for the publications
 * @param name Name of the input
 * @param id Orkg ID of the input predicate
 * @param inputType Input Type
 * @param datatype data type
 * @param _class Class
 */
export function createResourceInput(name: string, id: string, value: string, inputType: string, datatype: string, _class: string, modified: boolean = true): ResourcesInput {
    return {
        value: value,
        id: id,
        instanceId: '',
        inputType: inputType,
        nodeType: NodeType.resources,
        name: name,
        dataType: datatype,
        class: _class,
        statementId: '',
        modified: modified,
        show: false,
        delete: false,
        options: [],
        optionsDict: {}
    }
}

/**
 * Create an Author object
 * @remark Author is a literal since the website does it that way. But once this is clear it may change
 * @param authorName name of the author
 * @returns ResourcesInput Object of the author
 */
export function createAuthor(authorName: string): ResourcesInput {
    return {
        value: formatAuthorName(authorName),
        id: 'P27',
        instanceId: '',
        inputType: 'select',
        nodeType: NodeType.literals,
        name: 'Authors',
        dataType: 'xsd:string',
        class: '',
        statementId: '',
        modified: true,
        show: true,
        delete: false,
        options: [],
        optionsDict: {}
    }
}

/**
 * Changes the format of the name from "LastName, Name" to "Name LastName"
 * @param name Author Name in the format LastName, Name
 * @returns name in format Name LastName
 */
function formatAuthorName(name: string): string {
    try {
        let parts = name.split(',');
        let lastName = parts[0].trim();
        let firstName = parts[1].trim();
        return `${firstName} ${lastName}`;
    } catch (error) {
        return name; // If the name is already formated
    }
}

/**
 * Gets the doi
 * 
 * @remarks
 * For the publications ORKG requires only the 10.*** part of the doi
 * Not the full link
 * Obviously this is not documented
 * 
 * @param text text with doi
 * @returns doi if found
 * 
 */
export function getDoi(text: string) {
    try {
        let doiIdPattern = /10\.\d{4,9}\/[-._()/:A-Za-z0-9]+/;

        // Temp Doi
        var doi = ""

        let match1 = doiIdPattern.exec(text);
        if (match1 !== null) {
            doi = match1[0];
        }
        return doi;
    } catch (e) {
        console.log(`Error ${e}`);
        return ""
    }
}



/**
 * Creates the orkg config based on the template component 
 * @param templateComponent Template componenent
 */
export function createOrkgConfig(templateComponent: Map<string, any>) {
    let orkgStore: { [key: string]: BaseInput | ResourcesInput } = {};
    for (let [key, value] of templateComponent) {
        if (!value.hasOwnProperty('path')) { continue; }
        let id = value.path.id;
        let name = value.path.label;
        let inputType = "select"
        // It is a literal
        if (value.hasOwnProperty('datatype')) {
            let dataType = `xsd:${value.datatype.label.toLowerCase()}`
            if (value.datatype.label.toLowerCase() === 'uri') { dataType = 'xsd:anyURI' }
            inputType = value.datatype.label.toLowerCase()
            orkgStore[name] = createLiteralInput(name, id, "", inputType, dataType, false);
        } else if (value.hasOwnProperty('class')) {
            // It is a resource
            let class_id = value.class.id;
            orkgStore[name] = createResourceInput(name, id, '', inputType, '', class_id, false);
        }
    }   
    // Create the reference to Betty Research Engine ------------
    
    let id = get(orkgApiConfig).orkgSubmitterProperty;
    let name = "orkgSubmitter";
    let inputType = "text"; // Just a filler
    let class_id = "Software"
    let resourceInput = createResourceInput(name, id, '', inputType, '', class_id, false);
    resourceInput.optionsDict[name] = get(orkgApiConfig).orkgBettyReference;
    resourceInput.value = name;
    resourceInput.modified=true;
    orkgStore[name] = resourceInput;
    
    // ----------------------------------------------------------

    return orkgStore;
}