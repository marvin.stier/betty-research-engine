import { createHash } from "crypto";
import fs from "fs";
import * as csv from "@fast-csv/parse";

export async function handle({ event, resolve }) {
  const LOG_PATH = process.env.LOG_PATH || "connection.log";
  const timestamp = new Date().toISOString();
  const hashedIpAddress = createHash("sha256")
    .update(event.getClientAddress())
    .digest("hex");
  const pageVisited = event.url.pathname;
  const log = `${timestamp},${hashedIpAddress},${pageVisited}\n`;

  if (event.url.pathname === "/") {
    fs.writeFile(
      LOG_PATH,
      "timestamp,hashedIpAddress,pageVisited\n",
      { flag: "wx" },
      (err) => {
        if (!err || err.code === "EEXIST") {
          fs.appendFile(LOG_PATH, log, (err) => {
            if (err) {
              console.error("Failed to log connection:", err);
            }
          });
        } else {
          console.error("Failed to create file:", err);
        }
      }
    );
  }

  if (event.url.pathname === "/logs") {
    return await new Response(fs.readFileSync(LOG_PATH));
  }

  if (event.url.pathname === "/visits") {
    let totalVisits = 0;
    await new Promise((resolve, reject) => {
      csv
        .parseFile(LOG_PATH, { headers: true })
        .on("data", () => totalVisits++)
        .on("end", resolve)
        .on("error", reject);
    });

    return new Response(
      JSON.stringify({
        totalVisits,
      })
    );
  }

  return await resolve(event);
}
