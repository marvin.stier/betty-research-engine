import * as cheerio from 'cheerio';
import * as bibtexParse from 'bibtex-parse';

/**
 * Searchs the h1, h2, h3, h4, span and p tags for an abstract
 * @param htmlString raw html to be parsed
 * @returns The abstract or ""
 */

export function searchAbstract(htmlString: string): string {
    const debug = false;
    const tags = ['h1', 'h2', 'h3', 'h4', 'span', 'p'];
    const $ = cheerio.load(htmlString);

    for (let tag of tags) {
        for (let el of $(tag)) {
            {
                if (!(
                    $(el).text().toLowerCase().includes('abstract') ||
                    $(el).text().toLowerCase().includes('summary')
                )) { continue; }
                debug && console.log(el);
                let parentElement = $(el);
                for (let depth = 0; depth < 10; depth++) {
                    parentElement = parentElement.parent();
                    debug && console.log('checking', parentElement.html());
                    const pElements = parentElement.find('p');
                    if (pElements.length <= 0) { continue; }
                    debug && console.log(`--- pElements found at depth ${depth} ---`);
                    for (let p of pElements) {
                        const text = $(p).text();
                        // console.log(text);
                        if (text.length <= 300) { continue; }
                        debug && console.log(
                            `text length is ${text.length}, assuming it's the abstract, returning.`
                        );
                        // maybe change to longest text overall
                        //TODO:combine multiple p elements, often abstract is spread across many of them
                        return text;

                    };
                    debug && console.log(`--- end depth ${depth} ---`);

                }

            };
        }
    }
    return "";
}

export function bibtexToBibJson(bibtexString: string) {
    return bibtexParse.entries(bibtexString);
}

export function hasDoi(text: string): boolean {
    let doiIdPattern = /10\.\d{4,9}\/[-._()/:A-Za-z0-9]+/;
    let match1 = doiIdPattern.exec(text);
    return match1 !== null
}