import { json, error } from '@sveltejs/kit';
import type { RequestEvent } from './$types';

import { searchAbstract, bibtexToBibJson, hasDoi } from './utils';


/**
 * 
 * @param requestEvent 
 * @returns Response
 * fetch html of url given as 'url' get parameter e.g. http://localhost:5174/api/extract-paper-from-url?url=<doiUrl>
 * 
 * working example: https://doi.org/10.1007/s10950-019-09842-1
 * not working examples (because they are checking if accessed by a browser): 
 * https://doi.org/10.1002/2017jb014038
 * https://doi.org/10.1145/3557915.3561036
 * 
 * 
 */
export async function GET(requestEvent: RequestEvent) {
    let url = requestEvent.url.searchParams.get('url')
    if (url === null) throw error(400, 'URL must not be null');
    if (!hasDoi(url)) throw error(400, 'URL must be a DOI');
    let bodyHtml: string = "" //containing the HTML of the requested URL
    let abstract: string = ""
    let bibtex: Object;
    try {
        let response = await fetch(url)
        bodyHtml = await response.text()
        abstract = searchAbstract(bodyHtml);
        let responseBibtex = await (await fetch(url, { headers: { "Accept": "application/x-bibtex" } })).text();
        bibtex = bibtexToBibJson(responseBibtex);
        if (bibtex[0] === undefined) { // If no bibtex exist for the given doi
            bibtex = [{}];
        }
        bibtex[0]["ABSTRACT"] = abstract
    }
    catch (err) {
        console.error(err)
        return json(err)
    }

    return json(bibtex[0]);
}
