import { writable, type Writable } from 'svelte/store';

export const searchTerm: Writable<string> = writable("");
